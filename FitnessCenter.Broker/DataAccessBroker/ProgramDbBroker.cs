﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Broker.DataAccessBroker
{
    public class ProgramDbBroker : ReadWriteDbBroker<ProgramEntity>
    {
        protected override void AddInsertParameters(SqlCommand cmd, ProgramEntity entity)
        {
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 30).Value = entity.Name;
            cmd.Parameters.Add("@details", SqlDbType.VarChar, 256).Value = entity.Details;
            cmd.Parameters.Add("@type", SqlDbType.VarChar, 20).Value = entity.Type;
            cmd.Parameters.Add("@price", SqlDbType.Float).Value = entity.Price;
            cmd.Parameters.Add("@trainerId", SqlDbType.BigInt).Value = entity.TrainerId;
            cmd.Parameters.Add("@id", SqlDbType.BigInt).Direction = ParameterDirection.Output;
        }

        protected override void AddUpdateParameters(SqlCommand cmd, ProgramEntity entity)
        {
            cmd.Parameters.Add("@id", SqlDbType.BigInt).Value = entity.Id;
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 30).Value = entity.Name;
            cmd.Parameters.Add("@details", SqlDbType.VarChar, 256).Value = entity.Details;
            cmd.Parameters.Add("@type", SqlDbType.VarChar, 20).Value = entity.Type;
            cmd.Parameters.Add("@price", SqlDbType.Float).Value = entity.Price;
            cmd.Parameters.Add("@trainerId", SqlDbType.BigInt).Value = entity.TrainerId;
        }

        protected override ProgramEntity ReadEntity(SqlDataReader reader)
        {
            var program = new ProgramEntity();

            program.Id = reader.GetInt64("Id");
            program.Name = reader.GetString("Name");
            program.Details = reader.GetString("Details");
            program.Type = reader.GetString("Type");
            program.Price = reader.GetDouble("Price");
            program.TrainerId = reader.GetInt64("TrainerId");

            return program;
        }
    }
}
