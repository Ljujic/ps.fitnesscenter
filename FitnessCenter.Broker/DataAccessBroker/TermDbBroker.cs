﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Broker.DataAccessBroker
{
    public class TermDbBroker : ReadWriteDbBroker<TermEntity>
    {
        protected override void AddInsertParameters(SqlCommand cmd, TermEntity entity)
        {
            cmd.Parameters.Add("@programId", SqlDbType.BigInt).Value = entity.ProgramId;
            cmd.Parameters.Add("@userId", SqlDbType.BigInt).Value = entity.UserId;
            cmd.Parameters.Add("@dateOfTerm", SqlDbType.DateTime).Value = entity.DateOfTerm;
            cmd.Parameters.Add("@id", SqlDbType.BigInt).Direction = ParameterDirection.Output;
        }

        protected override void AddUpdateParameters(SqlCommand cmd, TermEntity entity)
        {
            cmd.Parameters.Add("@id", SqlDbType.BigInt).Value = entity.Id;
            cmd.Parameters.Add("@programId", SqlDbType.BigInt).Value = entity.ProgramId;
            cmd.Parameters.Add("@userId", SqlDbType.BigInt).Value = entity.UserId;
            cmd.Parameters.Add("@dateOfTerm", SqlDbType.DateTime).Value = entity.DateOfTerm;
        }

        protected override TermEntity ReadEntity(SqlDataReader reader)
        {
            var term = new TermEntity();

            term.Id = reader.GetInt64("Id");
            term.DateOfTerm = reader.GetDateTime("DateOfTerm");
            term.ProgramId = reader.GetInt64("ProgramId");
            term.UserId = reader.GetInt64("UserId");

            return term;
        }

        private TermEntity ReadUserTerm(SqlDataReader reader)
        {
            var term = new TermEntity();

            term.Id = reader.GetInt64("Id");
            term.DateOfTerm = reader.GetDateTime("DateOfTerm");
            term.Program = new ProgramEntity { Type=reader.GetString("ProgramType"), Name=reader.GetString("ProgramName"), Price=reader.GetDouble("ProgramPrice") };

            return term;
        }

        public async Task<IEnumerable<TermEntity>> FindUserTerms(long userId, DateTime startDate, DateTime endDate, SqlConnection conn, string commandText, CommandType commandType)
        {
            var result = new List<TermEntity>();

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@userId", SqlDbType.BigInt).Value = userId;
                cmd.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
                cmd.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var term = ReadUserTerm(reader);
                        result.Add(term);
                    }
                }
            }

            return result;
        }
    }
}
