﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Broker.DataAccessBroker
{
    public class PurchaseOrderDbBroker : ReadWriteDbBroker<PurchaseOrderEntity>
    {
        protected override void AddInsertParameters(SqlCommand cmd, PurchaseOrderEntity entity)
        {
            cmd.Parameters.Add("@totalPrice", SqlDbType.Float).Value = entity.TotalPrice;
            cmd.Parameters.Add("@dateOfDispatch", SqlDbType.DateTime).Value = entity.DateOfDispatch;
            cmd.Parameters.Add("@fitnessCenterId", SqlDbType.BigInt).Value = entity.FitnessCenterId;
            cmd.Parameters.Add("@userId", SqlDbType.BigInt).Value = entity.UserId;
            cmd.Parameters.Add("@id", SqlDbType.BigInt).Direction = ParameterDirection.Output;
        }

        protected override void AddUpdateParameters(SqlCommand cmd, PurchaseOrderEntity entity)
        {
            cmd.Parameters.Add("@id", SqlDbType.BigInt).Value = entity.Id;
            cmd.Parameters.Add("@totalPrice", SqlDbType.Float).Value = entity.TotalPrice;
            cmd.Parameters.Add("@dateOfDispatch", SqlDbType.DateTime).Value = entity.DateOfDispatch;
            cmd.Parameters.Add("@fitnessCenterId", SqlDbType.BigInt).Value = entity.FitnessCenterId;
            cmd.Parameters.Add("@userId", SqlDbType.BigInt).Value = entity.UserId;
        }

        protected override PurchaseOrderEntity ReadEntity(SqlDataReader reader)
        {
            var purchaseOrder = new PurchaseOrderEntity();

            purchaseOrder.Id = reader.GetInt64("Id");
            purchaseOrder.TotalPrice = reader.GetDouble("TotalPrice");
            purchaseOrder.DateOfDispatch = reader.GetDateTime("DateOfDispatch");
            purchaseOrder.FitnessCenterId = reader.GetInt64("FitnessCenterId");
            purchaseOrder.UserId = reader.GetInt64("UserId");

            return purchaseOrder;
        }

        public async Task<IEnumerable<PurchaseOrderEntity>> ReadUserPurchaseOrdersAsync(long userId, SqlConnection conn, string commandText, CommandType commandType)
        {
            var userPurchaseOrders = new List<PurchaseOrderEntity>();

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@userId", SqlDbType.BigInt).Value = userId;

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var userPurchaseOrder = ReadEntity(reader);
                        userPurchaseOrders.Add(userPurchaseOrder);
                    }
                }
            }

            return userPurchaseOrders;
        }

        public async Task UpdateProductQuantity(long productId, int amount, SqlConnection conn, string commandText, CommandType commandType)
        {
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@productId", SqlDbType.BigInt).Value = productId;
                cmd.Parameters.Add("@amount", SqlDbType.Int).Value = amount;

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task<IEnumerable<PurchaseOrderItemEntity>> GetPurchaseOrderItems(long id, SqlConnection conn, string commandText, CommandType commandType)
        {
            var purchaseOrderItems = new List<PurchaseOrderItemEntity>();

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@purchaseOrderId", SqlDbType.BigInt).Value = id;

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var purchaseOrderItem = ReadPurchaseOrderItem(reader);
                        purchaseOrderItems.Add(purchaseOrderItem);
                    }
                }
            }

            return purchaseOrderItems;
        }

        public async Task DeletePurchaseOrderItemAsync(long id, SqlConnection conn, string commandText, CommandType commandType)
        {
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@id", SqlDbType.BigInt).Value = id;

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task<PurchaseOrderItemEntity> InsertPurchaseOrderItemAsync(PurchaseOrderItemEntity poItem, SqlConnection conn, string commandText, CommandType commandType)
        {
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@purchaseOrderId", SqlDbType.BigInt).Value = poItem.PurchaseOrderId;
                cmd.Parameters.Add("@amount", SqlDbType.Int).Value = poItem.Amount;
                cmd.Parameters.Add("@price", SqlDbType.Float).Value = poItem.Price;
                cmd.Parameters.Add("@productId", SqlDbType.BigInt).Value = poItem.ProductId;
                cmd.Parameters.Add("@id", SqlDbType.BigInt).Direction = ParameterDirection.Output;

                await cmd.ExecuteNonQueryAsync();

                poItem.Id = (long)cmd.Parameters["@id"].Value;
            }

            return poItem;
        }

        private PurchaseOrderItemEntity ReadPurchaseOrderItem(SqlDataReader reader)
        {
            var purchaseOrderItem = new PurchaseOrderItemEntity();

            purchaseOrderItem.Id = reader.GetInt64("Id");
            purchaseOrderItem.PurchaseOrderId = reader.GetInt64("PurchaseOrderId");
            purchaseOrderItem.Amount = reader.GetInt32("Amount");
            purchaseOrderItem.Price = reader.GetDouble("Price");
            purchaseOrderItem.ProductId = reader.GetInt64("ProductId");

            return purchaseOrderItem;
        }
    }
}
