﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Broker.DataAccessBroker
{
    public class ProductDbBroker : ReadDbBroker<ProductEntity>
    {
        protected override ProductEntity ReadEntity(SqlDataReader reader)
        {
            var product = new ProductEntity();

            product.Id = reader.GetInt64("Id");
            product.Name = reader.GetString("Name");
            product.Details = reader.GetString("Details");
            product.Price = reader.GetDouble("Price");
            product.Quantity = reader.GetInt32("Quantity");

            return product;
        }
    }
}
