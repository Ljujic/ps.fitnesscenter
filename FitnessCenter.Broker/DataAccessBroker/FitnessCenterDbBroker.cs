﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Broker.DataAccessBroker
{
    public class FitnessCenterDbBroker : ReadWriteDbBroker<FitnessCenterEntity>
    {
        protected override void AddInsertParameters(SqlCommand cmd, FitnessCenterEntity entity)
        {
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 30).Value = entity.Name;
            cmd.Parameters.Add("@type", SqlDbType.VarChar, 20).Value = entity.Type;
            cmd.Parameters.Add("@phoneNumber", SqlDbType.VarChar, 20).Value = entity.PhoneNumber;
            cmd.Parameters.Add("@details", SqlDbType.VarChar, 256).Value = entity.Details;
            cmd.Parameters.Add("@address", SqlDbType.VarChar, 30).Value = entity.Address;
            cmd.Parameters.Add("@municipalityId", SqlDbType.BigInt).Value = entity.MunicipalityId;
            cmd.Parameters.Add("@id", SqlDbType.BigInt).Direction = ParameterDirection.Output;
        }

        protected override void AddUpdateParameters(SqlCommand cmd, FitnessCenterEntity entity)
        {
            cmd.Parameters.Add("@id", SqlDbType.BigInt).Value = entity.Id;
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 30).Value = entity.Name;
            cmd.Parameters.Add("@type", SqlDbType.VarChar, 20).Value = entity.Type;
            cmd.Parameters.Add("@phoneNumber", SqlDbType.VarChar, 20).Value = entity.PhoneNumber;
            cmd.Parameters.Add("@details", SqlDbType.VarChar, 256).Value = entity.Details;
            cmd.Parameters.Add("@address", SqlDbType.VarChar, 30).Value = entity.Address;
            cmd.Parameters.Add("@municipalityId", SqlDbType.BigInt).Value = entity.MunicipalityId;
        }


        protected override FitnessCenterEntity ReadEntity(SqlDataReader reader)
        {
            var fitnessCenter = new FitnessCenterEntity();

            fitnessCenter.Id = reader.GetInt64("Id");
            fitnessCenter.Name = reader.GetString("Name");
            fitnessCenter.Type = reader.GetString("Type");
            fitnessCenter.PhoneNumber = reader.GetString("PhoneNumber");
            fitnessCenter.Details = reader.GetString("Details");
            fitnessCenter.Address = reader.GetString("Address");
            fitnessCenter.MunicipalityId = reader.GetInt64("MunicipalityId");

            return fitnessCenter;
        }

        public async Task<List<TrainerEntity>> GetFitnessCenterTrainers(long id, SqlConnection conn, string commandText, CommandType commandType)
        {
            var fitnessCenterTrainers = new List<TrainerEntity>();

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@fitnessCenterId", SqlDbType.BigInt).Value = id;

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var fitnessCenterTrainer = ReadFitnessCenterTrainer(reader);
                        fitnessCenterTrainers.Add(fitnessCenterTrainer);
                    }
                }
            }

            return fitnessCenterTrainers;
        }

        public async Task<List<UserEntity>> GetFitnessCenterUsers(long id, SqlConnection conn, string commandText, CommandType commandType)
        {
            var fitnessCenterUsers = new List<UserEntity>();

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@fitnessCenterId", SqlDbType.BigInt).Value = id;

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var fitnessCenterUser = ReadFitnessCenterUser(reader);
                        fitnessCenterUsers.Add(fitnessCenterUser);
                    }
                }
            }

            return fitnessCenterUsers;
        }

        public async Task InsertProgramTrainer(ProgramEntity program, SqlConnection conn, string commandText, CommandType commandType)
        {
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = program.Name;
                cmd.Parameters.Add("@details", SqlDbType.VarChar).Value = program.Details;
                cmd.Parameters.Add("@type", SqlDbType.VarChar).Value = program.Type;
                cmd.Parameters.Add("@trainerId", SqlDbType.BigInt).Value = program.TrainerId;
                cmd.Parameters.Add("@price", SqlDbType.Float).Value = program.Price;
                cmd.Parameters.Add("@id", SqlDbType.BigInt).Direction = ParameterDirection.Output;

                await cmd.ExecuteNonQueryAsync();
                program.Id = (long)cmd.Parameters["@id"].Value;
            }
        }

        public async Task<TrainerEntity> InsertFitnessCenterTrainer(long fitnessCenterId, TrainerEntity entity, SqlConnection conn, string commandText, CommandType commandType)
        {
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@fitnessCenterId", SqlDbType.BigInt).Value = fitnessCenterId;
                cmd.Parameters.Add("@nameSurname", SqlDbType.VarChar, 128).Value = entity.NameSurname;
                cmd.Parameters.Add("@username", SqlDbType.VarChar, 20).Value = entity.Username;
                cmd.Parameters.Add("@password", SqlDbType.VarChar, 20).Value = entity.Password;
                cmd.Parameters.Add("@address", SqlDbType.VarChar, 30).Value = entity.Address;
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 40).Value = entity.Email;
                cmd.Parameters.Add("@reference", SqlDbType.VarChar, 256).Value = entity.Reference;
                cmd.Parameters.Add("@id", SqlDbType.BigInt).Direction = ParameterDirection.Output;

                await cmd.ExecuteNonQueryAsync();
                entity.Id = (long)cmd.Parameters["@id"].Value;
            }

            return entity;
        }

        public async Task InsertFitnessCenterProduct(long fitnessCenterId, ProductEntity entity, SqlConnection conn, string commandText, CommandType commandType)
        {
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@fitnessCenterId", SqlDbType.BigInt).Value = fitnessCenterId;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 60).Value = entity.Name;
                cmd.Parameters.Add("@details", SqlDbType.VarChar, 128).Value = entity.Details;
                cmd.Parameters.Add("@price", SqlDbType.Float).Value = entity.Price;
                cmd.Parameters.Add("@quantity", SqlDbType.Int).Value = entity.Quantity;
                cmd.Parameters.Add("@id", SqlDbType.BigInt).Direction = ParameterDirection.Output;

                await cmd.ExecuteNonQueryAsync();
                entity.Id = (long)cmd.Parameters["@id"].Value;
            }
        }

        public async Task<List<ProductEntity>> GetFitnessCenterProducts(long id, SqlConnection conn, string commandText, CommandType commandType)
        {
            var fitnessCenterProducts = new List<ProductEntity>();

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@fitnessCenterId", SqlDbType.BigInt).Value = id;

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var fitnessCenterProduct = ReadFitnessCenterProduct(reader);
                        fitnessCenterProducts.Add(fitnessCenterProduct);
                    }
                }
            }

            return fitnessCenterProducts;
        }

        private ProductEntity ReadFitnessCenterProduct(SqlDataReader reader)
        {
            var fitnessCenterProduct = new ProductEntity();

            fitnessCenterProduct.Id = reader.GetInt64("Id");
            fitnessCenterProduct.Name = reader.GetString("Name");
            fitnessCenterProduct.Details = reader.GetString("Details");
            fitnessCenterProduct.Price = reader.GetDouble("Price");
            fitnessCenterProduct.Quantity = reader.GetInt32("Quantity");

            return fitnessCenterProduct;
        }

        private UserEntity ReadFitnessCenterUser(SqlDataReader reader)
        {
            var fitnessCenterUser = new UserEntity();

            fitnessCenterUser.Id = reader.GetInt64("Id");
            fitnessCenterUser.Email = reader.GetString("Email");
            fitnessCenterUser.NameSurname = reader.GetString("NameSurname");
            fitnessCenterUser.Address = reader.GetString("Address");
            fitnessCenterUser.DateOfBirth = reader.GetDateTime("DateOfBirth");
            fitnessCenterUser.Username = reader.GetString("Username");
            fitnessCenterUser.Password = reader.GetString("Password");
            fitnessCenterUser.BankCardBalance = reader.GetDouble("BankCardBalance");

            return fitnessCenterUser;
        }

        private TrainerEntity ReadFitnessCenterTrainer(SqlDataReader reader)
        {
            var fitnessCenterTrainer = new TrainerEntity();

            fitnessCenterTrainer.Id = reader.GetInt64("Id");
            fitnessCenterTrainer.NameSurname = reader.GetString("NameSurname");
            fitnessCenterTrainer.Email = reader.GetString("Email");
            fitnessCenterTrainer.Username = reader.GetString("Username");
            fitnessCenterTrainer.Password = reader.GetString("Password");
            fitnessCenterTrainer.Address = reader.GetString("Address");
            fitnessCenterTrainer.Reference = reader.GetString("Reference");

            return fitnessCenterTrainer;
        }
    }
}
