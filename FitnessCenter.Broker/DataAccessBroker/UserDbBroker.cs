﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Broker.DataAccessBroker
{
    public class UserDbBroker : ReadWriteDbBroker<UserEntity>
    {
        public async Task<UserEntity> FindUserByUsernameOrEmail(string username, string email, SqlConnection conn, string commandText, CommandType commandType)
        {
            var userData = default(UserEntity);

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@username", SqlDbType.VarChar, 20).Value = username;
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 40).Value = email;

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (await reader.ReadAsync())
                    {
                        userData = ReadEntity(reader);
                    }
                }
            }

            return userData;
        }

        public async Task PayPurchasePrice(long userId, double totalPrice, SqlConnection conn, string commandText, CommandType commandType)
        {
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@id", SqlDbType.BigInt).Value = userId;
                cmd.Parameters.Add("@totalPrice", SqlDbType.Float).Value = totalPrice;

                await cmd.ExecuteNonQueryAsync();
            }
        }

        protected override void AddInsertParameters(SqlCommand cmd, UserEntity entity)
        {
            cmd.Parameters.Add("@nameSurname", SqlDbType.VarChar, 128).Value = entity.NameSurname;
            cmd.Parameters.Add("@username", SqlDbType.VarChar, 20).Value = entity.Username;
            cmd.Parameters.Add("@password", SqlDbType.VarChar, 200).Value = entity.Password;
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 30).Value = entity.Email;
            cmd.Parameters.Add("@address", SqlDbType.VarChar, 30).Value = entity.Address;
            cmd.Parameters.Add("@dateOfBirth", SqlDbType.DateTime).Value = entity.DateOfBirth;
            cmd.Parameters.Add("@bankCardBalance", SqlDbType.Float).Value = entity.BankCardBalance;
            cmd.Parameters.Add("@fitnessCenterId", SqlDbType.BigInt).Value = entity.FitnessCenterId;
            cmd.Parameters.Add("@id", SqlDbType.BigInt).Direction = ParameterDirection.Output;
        }

        public async Task DeletePaidPurchaseOrderItem(long purchaseOrderId, long poItemId, SqlConnection conn, string commandText, CommandType commandType)
        {
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@purchaseOrderId", SqlDbType.BigInt).Value = purchaseOrderId;
                cmd.Parameters.Add("@poItemId", SqlDbType.BigInt).Value = poItemId;
                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task DeletePaidPurchaseOrder(long id, SqlConnection conn, string commandText, CommandType commandType)
        {
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.Add("@purchaseOrderId", SqlDbType.BigInt).Value = id;
                await cmd.ExecuteNonQueryAsync();
            }
        }

        protected override void AddUpdateParameters(SqlCommand cmd, UserEntity entity)
        {
            cmd.Parameters.Add("@id", SqlDbType.BigInt).Value= entity.Id;
            cmd.Parameters.Add("@nameSurname", SqlDbType.VarChar, 128).Value = entity.NameSurname;
            cmd.Parameters.Add("@username", SqlDbType.VarChar, 20).Value = entity.Username;
            cmd.Parameters.Add("@password", SqlDbType.VarChar, 20).Value = entity.Password;
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 30).Value = entity.Email;
            cmd.Parameters.Add("@address", SqlDbType.VarChar, 30).Value = entity.Address;
            cmd.Parameters.Add("@dateOfBirth", SqlDbType.DateTime).Value = entity.DateOfBirth;
            cmd.Parameters.Add("@bankCardBalance", SqlDbType.Float).Value = entity.BankCardBalance;
            cmd.Parameters.Add("@fitnessCenterId", SqlDbType.BigInt).Value = entity.FitnessCenterId;
        }

        protected override UserEntity ReadEntity(SqlDataReader reader)
        {
            var user = new UserEntity();

            user.Id = reader.GetInt64("Id");
            user.NameSurname = reader.GetString("NameSurname");
            user.Username = reader.GetString("Username");
            user.Password = reader.GetString("Password");
            user.Email = reader.GetString("Email");
            user.Address = reader.GetString("Address");
            user.DateOfBirth = reader.GetDateTime("DateOfBirth");
            user.BankCardBalance = reader.GetDouble("BankCardBalance");
            user.FitnessCenterId = reader.GetInt64("FitnessCenterId");

            return user;
        }
    }
}
