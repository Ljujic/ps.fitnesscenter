﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Broker.DataAccessBroker
{
    public class MunicipalityDbBroker : ReadDbBroker<MunicipalityEntity>
    {
        protected override MunicipalityEntity ReadEntity(SqlDataReader reader)
        {
            var municipality = new MunicipalityEntity();

            municipality.Id = reader.GetInt64("Id");
            municipality.Name = reader.GetString("Name");
            municipality.ZipCode = reader.GetString("ZipCode");

            return municipality;
        }
    }
}
