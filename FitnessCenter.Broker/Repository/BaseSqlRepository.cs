﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Broker.Repository
{
    public abstract class BaseSqlRepository
    {
        public string ConnectionString { get; set; }

        public BaseSqlRepository(IOptions<DbSettings> dbSettings)
        {
            ConnectionString = dbSettings.Value.ConnectionString;
        }
    }
}
