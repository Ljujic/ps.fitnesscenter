﻿using FitnessCenter.Broker.DataAccessBroker;
using FitnessCenter.Domain;
using FitnessCenter.Repository;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace FitnessCenter.Broker.Repository
{
    public class UserRepository : BaseSqlRepository, IUserRepository
    {
        private readonly UserDbBroker _userDbBroker;

        public UserRepository(IOptions<DbSettings> dbSettings) : base(dbSettings)
        {
            _userDbBroker = new UserDbBroker();
        }

        public async Task<UserEntity> FindUserByUsernameOrEmail(string username, string email)
        {
            var user = default(UserEntity); 

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                user = await _userDbBroker.FindUserByUsernameOrEmail(username, email, conn, "[dbo].[FindUserByUsernameOrEmail]", CommandType.StoredProcedure);
            }

            return user;
        }

        public async Task<UserEntity> InsertAsync(UserEntity entity)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();
                    entity = await _userDbBroker.InsertAsync(entity, conn, "[dbo].[InsertUser]", CommandType.StoredProcedure);
                }

                ts.Complete();
            }

            return entity;
        }

        public async Task<UserEntity> PayPurchasePrice(PurchaseOrderEntity purchaseOrder)
        {
            var result = default(UserEntity);

            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();

                    await _userDbBroker.PayPurchasePrice(purchaseOrder.UserId, purchaseOrder.TotalPrice, conn, "[dbo].[PayPurchaseOrder]", CommandType.StoredProcedure);
                    await _userDbBroker.DeletePaidPurchaseOrder(purchaseOrder.Id, conn, "[dbo].[DeletePaidPurchaseOrder]", CommandType.StoredProcedure);

                    foreach (var poItem in purchaseOrder.PurchaseOrderItems)
                    {
                        await _userDbBroker.DeletePaidPurchaseOrderItem(purchaseOrder.Id, poItem.Id, conn, "[dbo].[DeletePaidPurchaseOrderItem]", CommandType.StoredProcedure);
                    }

                    result = await _userDbBroker.GetByIdAsync(purchaseOrder.UserId, conn, "[dbo].[GetUserById]", CommandType.StoredProcedure);
                }

                ts.Complete();
            }

            return result;
        }
    }
}
