﻿using FitnessCenter.Broker.DataAccessBroker;
using FitnessCenter.Domain;
using FitnessCenter.Repository;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Broker.Repository
{
    public class MunicipalityRepository : BaseSqlRepository, IMunicipalityRepository
    {
        private readonly MunicipalityDbBroker _municipalityDbBroker;

        public MunicipalityRepository(IOptions<DbSettings> dbSettings) : base(dbSettings)
        {
            _municipalityDbBroker = new MunicipalityDbBroker();
        }

        public async Task<IEnumerable<MunicipalityEntity>> GetAllAsync()
        {
            var result = default(IEnumerable<MunicipalityEntity>);

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                result = await _municipalityDbBroker.GetAllAsync(conn, "[dbo].[GetAllMunicipalities]", CommandType.StoredProcedure);
            }

            return result;
        }
    }
}
