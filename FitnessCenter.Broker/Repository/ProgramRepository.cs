﻿using FitnessCenter.Broker.DataAccessBroker;
using FitnessCenter.Domain;
using FitnessCenter.Repository;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace FitnessCenter.Broker.Repository
{
    public class ProgramRepository : BaseSqlRepository, IProgramRepository
    {
        private readonly ProgramDbBroker _programDbBroker;

        public ProgramRepository(IOptions<DbSettings> dbSettings) : base(dbSettings)
        {
            _programDbBroker = new ProgramDbBroker();
        }

        public async Task<IEnumerable<ProgramEntity>> GetAllAsync()
        {
            var result = default(IEnumerable<ProgramEntity>);

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                result = await _programDbBroker.GetAllAsync(conn, "[dbo].[GetAllPrograms]", CommandType.StoredProcedure);
            }

            return result;
        }

        public async Task<ProgramEntity> InsertAsync(ProgramEntity entity)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) 
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();
                    entity = await _programDbBroker.InsertAsync(entity, conn, "[dbo].[InsertProgram]", CommandType.StoredProcedure);
                }

                ts.Complete();
            }

            return entity;
        }
    }
}
