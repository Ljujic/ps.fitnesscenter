﻿using FitnessCenter.Broker.DataAccessBroker;
using FitnessCenter.Domain;
using FitnessCenter.Repository;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace FitnessCenter.Broker.Repository
{
    public class FitnessCenterRepository : BaseSqlRepository, IFitnessCenterRepository
    {
        private readonly FitnessCenterDbBroker _fitnessCenterDbBroker;

        public FitnessCenterRepository(IOptions<DbSettings> dbSettings) : base(dbSettings)
        {
            _fitnessCenterDbBroker = new FitnessCenterDbBroker();
        }

        public async Task<bool> DeleteAsync(long id)
        {
            bool result = false;

            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();
                    result = await _fitnessCenterDbBroker.DeleteAsync(id, conn, "[dbo].[DeleteFitnessCenter]", CommandType.StoredProcedure);
                }

                ts.Complete();
            }

            return result;
        }

        public async Task<IEnumerable<FitnessCenterEntity>> GetAllAsync()
        {
            var result = default(IEnumerable<FitnessCenterEntity>);

            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();
                    result = await _fitnessCenterDbBroker.GetAllAsync(conn, "[dbo].[GetAllFitnessCenters]", CommandType.StoredProcedure);

                    foreach (var fitnessCenter in result)
                    {
                        fitnessCenter.Trainers = await _fitnessCenterDbBroker.GetFitnessCenterTrainers(fitnessCenter.Id, conn, "[dbo].[GetFitnessCenterTrainers]", CommandType.StoredProcedure);
                        fitnessCenter.Users = await _fitnessCenterDbBroker.GetFitnessCenterUsers(fitnessCenter.Id, conn, "[dbo].[GetFitnessCenterUsers]", CommandType.StoredProcedure);
                        fitnessCenter.Products = await _fitnessCenterDbBroker.GetFitnessCenterProducts(fitnessCenter.Id, conn, "[dbo].[GetFitnessCenterProducts]", CommandType.StoredProcedure);
                    }
                }

                ts.Complete();
            }

            return result;
        }

        public async Task<FitnessCenterEntity> InsertAsync(FitnessCenterEntity entity)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();
                    entity = await _fitnessCenterDbBroker.InsertAsync(entity, conn, "[dbo].[InsertFitnessCenter]", CommandType.StoredProcedure);

                    //inserting fitness center products
                    foreach (var product in entity.Products)
                        await _fitnessCenterDbBroker.InsertFitnessCenterProduct(entity.Id, product, conn, "[dbo].[InsertProduct]", CommandType.StoredProcedure);

                    //inserting fitness center trainers
                    foreach (var trainer in entity.Trainers)
                    { 
                       var newTrainer = await _fitnessCenterDbBroker.InsertFitnessCenterTrainer(entity.Id, trainer, conn, "[dbo].[InsertTrainer]", CommandType.StoredProcedure);
                        foreach (var program in newTrainer.Programs)
                        {
                            program.TrainerId = newTrainer.Id;
                            await _fitnessCenterDbBroker.InsertProgramTrainer(program, conn, "[dbo].[InsertProgramTrainer]", CommandType.StoredProcedure);
                        } 
                    }
                }

                ts.Complete();
            }

            return entity;
        } 
    }
}
