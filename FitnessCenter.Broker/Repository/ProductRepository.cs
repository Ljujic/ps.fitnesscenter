﻿using FitnessCenter.Broker.DataAccessBroker;
using FitnessCenter.Domain;
using FitnessCenter.Repository;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Broker.Repository
{
    public class ProductRepository : BaseSqlRepository, IProductRepository
    {
        private readonly ProductDbBroker _productDbBroker;

        public ProductRepository(IOptions<DbSettings> dbSettings) : base(dbSettings)
        {
            _productDbBroker = new ProductDbBroker();
        }

        public async Task<IEnumerable<ProductEntity>> GetAllAsync()
        {
            var result = default(IEnumerable<ProductEntity>);

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                result = await _productDbBroker.GetAllAsync(conn, "[dbo].[GetAllProducts]", CommandType.StoredProcedure);
            }

            return result;
        }
    }
}
