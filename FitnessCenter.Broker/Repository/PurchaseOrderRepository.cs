﻿using FitnessCenter.Broker.DataAccessBroker;
using FitnessCenter.Domain;
using FitnessCenter.Repository;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace FitnessCenter.Broker.Repository
{
    public class PurchaseOrderRepository : BaseSqlRepository, IPurchaseOrderRepository
    {
        private readonly PurchaseOrderDbBroker _purchaseOrderDbBroker;

        public PurchaseOrderRepository(IOptions<DbSettings> dbSettings) : base(dbSettings)
        {
            _purchaseOrderDbBroker = new PurchaseOrderDbBroker();
        }

        public async Task<IEnumerable<PurchaseOrderEntity>> GetAllAsync()
        {
            var result = default(IEnumerable<PurchaseOrderEntity>);

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                result = await _purchaseOrderDbBroker.GetAllAsync(conn, "[dbo].[GetAllPurchaseOrders]", CommandType.StoredProcedure);
            }

            return result;
        }

        public async Task<PurchaseOrderEntity> InsertAsync(PurchaseOrderEntity entity)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();
                    entity = await _purchaseOrderDbBroker.InsertAsync(entity, conn, "[dbo].[InsertUserPurchaseOrder]", CommandType.StoredProcedure);

                    foreach (var poItem in entity.PurchaseOrderItems)
                    {
                        poItem.PurchaseOrderId = entity.Id;
                        await _purchaseOrderDbBroker.InsertPurchaseOrderItemAsync(poItem, conn, "[dbo].[InsertPurchaseOrderItem]", CommandType.StoredProcedure);

                        await _purchaseOrderDbBroker.UpdateProductQuantity(poItem.ProductId, poItem.Amount, conn, "[dbo].[UpdateProductQuantity]", CommandType.StoredProcedure);
                    }
                }

                ts.Complete();
            }

            return entity;
        }

        public async Task<IEnumerable<PurchaseOrderEntity>> ReadUserPurchaseOrdersAsync(long userId)
        {
            var result = default(IEnumerable<PurchaseOrderEntity>);

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                result = await _purchaseOrderDbBroker.ReadUserPurchaseOrdersAsync(userId, conn, "[dbo].[GetUserPurchaseOrders]", CommandType.StoredProcedure);

                foreach (var purchaseOrder in result)
                {
                    purchaseOrder.PurchaseOrderItems = (List<PurchaseOrderItemEntity>)await _purchaseOrderDbBroker.GetPurchaseOrderItems(purchaseOrder.Id, conn, "[dbo].[GetPurchaseOrderItems]", CommandType.StoredProcedure);
                }
            }

            return result;
        }

        public async Task<PurchaseOrderEntity> UpdateAsync(PurchaseOrderEntity entity)
        {
            //TODO : update purchase order item table
            var currentPurchaseOrderItems = default(IEnumerable<PurchaseOrderItemEntity>);
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                currentPurchaseOrderItems = await _purchaseOrderDbBroker.GetPurchaseOrderItems(entity.Id, conn, "[dbo].[GetPurchaseOrderItems]", CommandType.StoredProcedure);
            }

            var purchaseOrderItemsToDelete = GetPurchaseOrderItemsToDelete(entity, currentPurchaseOrderItems);
            var purchaseOrderItemsToInsert = GetPurchaseOrderItemsToInsert(entity, currentPurchaseOrderItems);

            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();

                    //update purchase order item
                    entity = await _purchaseOrderDbBroker.UpdateAsync(entity, conn, "[dbo].[UpdatePurchaseOrder]", CommandType.StoredProcedure);

                    //delete purchase order item
                    foreach (var id in purchaseOrderItemsToDelete)
                    {
                        await _purchaseOrderDbBroker.DeletePurchaseOrderItemAsync(id, conn, "[dbo].[DeletePurchaseOrderItem]", CommandType.StoredProcedure);
                    }

                    //insert purchase order item
                    foreach (var poItem in purchaseOrderItemsToInsert)
                    {
                        await _purchaseOrderDbBroker.InsertPurchaseOrderItemAsync(poItem, conn, "[dbo].[InsertPurchaseOrderItem]", CommandType.StoredProcedure);
                    }

                    //refetch in order to update collection
                    entity = await _purchaseOrderDbBroker.GetByIdAsync(entity.Id, conn, "[dbo].[GetPurchaseOrderById]", CommandType.StoredProcedure);
                }

                ts.Complete();
            }

            return entity;
        }

        private IEnumerable<long> GetPurchaseOrderItemsToDelete(PurchaseOrderEntity entity, IEnumerable<PurchaseOrderItemEntity> currentPurchaseOrderItems)
        {
            var itemIdsToDelete = new List<long>();

            foreach (var poItem in currentPurchaseOrderItems)
            {
                if (!entity.PurchaseOrderItems.Any(i => i.Id == poItem.Id))
                    itemIdsToDelete.Add(poItem.Id);
            }

            return itemIdsToDelete;
        }

        private IEnumerable<PurchaseOrderItemEntity> GetPurchaseOrderItemsToInsert(PurchaseOrderEntity entity, IEnumerable<PurchaseOrderItemEntity> currentPurchaseOrderItems)
        {
            var itemsToInsert = new List<PurchaseOrderItemEntity>();

            foreach (var poItem in entity.PurchaseOrderItems)
            {
                if (!currentPurchaseOrderItems.Any(i => i.Id == poItem.Id))
                    itemsToInsert.Add(poItem);
            }

            return itemsToInsert;
        }        
    }
}
