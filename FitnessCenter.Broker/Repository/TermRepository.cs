﻿using FitnessCenter.Broker.DataAccessBroker;
using FitnessCenter.Domain;
using FitnessCenter.Repository;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace FitnessCenter.Broker.Repository
{
    public class TermRepository : BaseSqlRepository, ITermRepository
    {
        private readonly TermDbBroker _termDbBroker;

        public TermRepository(IOptions<DbSettings> dbSettings) : base(dbSettings)
        {
            _termDbBroker = new TermDbBroker();
        }

        public async Task<bool> DeleteAsync(long id)
        {
            bool result = false;
            
            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();
                    result = await _termDbBroker.DeleteAsync(id, conn, "[dbo].[DeleteTerm]", CommandType.StoredProcedure);
                }

                ts.Complete();
            }

            return result;
        }

        public async Task<IEnumerable<TermEntity>> FindUserTermsAsync(long userId, DateTime startDate, DateTime endDate)
        {
            var result = default(IEnumerable<TermEntity>);

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                result = await _termDbBroker.FindUserTerms(userId, startDate, endDate, conn, "[dbo].[FindTerms]", CommandType.StoredProcedure);
            }

            return result;
        }

        public async Task<TermEntity> InsertAsync(TermEntity entity)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();
                    entity = await _termDbBroker.InsertAsync(entity, conn, "[dbo].[InsertTerm]", CommandType.StoredProcedure);
                }

                ts.Complete();
            }

            return entity;
        }
    }
}
