﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Domain
{
    [Serializable]
    public class PurchaseOrderItemEntity : IEntity
    {
        public long Id { get; set; }
        public long PurchaseOrderId { get; set; }
        public int Amount { get; set; }
        public double Price { get; set; }
        public long ProductId { get; set; }
    }
}
