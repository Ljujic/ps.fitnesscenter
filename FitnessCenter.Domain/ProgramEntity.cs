﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Domain
{
    [Serializable]
    public class ProgramEntity : IEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string Type { get; set; }
        public double Price { get; set; }
        public long TrainerId { get; set; }
    }
}
