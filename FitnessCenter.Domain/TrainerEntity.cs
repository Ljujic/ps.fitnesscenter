﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Domain
{
    [Serializable]
    public class TrainerEntity : IEntity
    {
        public long Id { get; set; }
        public string NameSurname { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string Reference { get; set; }
        public long FitnessCenterId { get; set; }
        public List<ProgramEntity> Programs { get; set; }
    }
}
