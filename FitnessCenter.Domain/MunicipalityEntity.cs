﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Domain
{
    [Serializable]
    public class MunicipalityEntity : IEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ZipCode { get; set; }
    }
}
