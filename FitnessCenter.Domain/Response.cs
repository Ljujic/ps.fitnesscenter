﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Domain
{
    [Serializable]
    public class Response
    {
        public SIGNAL Signal { get; set; } = SIGNAL.Ok;
        public string Message { get; set; }
        public object Object { get; set; }
    }

    public enum SIGNAL
    {
        Ok,
        Error
    }
}
