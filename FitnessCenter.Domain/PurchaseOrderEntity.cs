﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Domain
{
    [Serializable]
    public class PurchaseOrderEntity : IEntity
    {
        public long Id { get; set; }
        public double TotalPrice { get; set; }
        public DateTime DateOfDispatch { get; set; }
        public long UserId { get; set; }
        public long FitnessCenterId { get; set; }
        public List<PurchaseOrderItemEntity> PurchaseOrderItems { get; set; }
    }
}
