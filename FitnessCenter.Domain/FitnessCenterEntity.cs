﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Domain
{
    [Serializable]
    public class FitnessCenterEntity : IEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string PhoneNumber { get; set; }
        public string Details { get; set; }
        public string Address { get; set; }
        public long MunicipalityId { get; set; }
        public List<ProgramEntity> Programs { get; set; }
        public List<TrainerEntity> Trainers { get; set; }
        public List<UserEntity> Users { get; set; }
        public List<ProductEntity> Products { get; set; }
    }
}
