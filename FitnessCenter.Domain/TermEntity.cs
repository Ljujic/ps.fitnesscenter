﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Domain
{
    [Serializable]
    public class TermEntity : IEntity
    {
        public long Id { get; set; }
        public long ProgramId { get; set; }
        public ProgramEntity Program { get; set; }
        public long UserId { get; set; }
        public DateTime DateOfTerm { get; set; }
        public DateTime EndDate { get; set; }
    }
}
