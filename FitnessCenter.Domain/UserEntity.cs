﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Domain
{
    [Serializable]
    public class UserEntity : IEntity
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string NameSurname { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public double BankCardBalance { get; set; }
        public long FitnessCenterId { get; set; }
    }
}
