﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Domain
{
    [Serializable]
    public class Request
    {
        public OPERATION Operation { get; set; }
        public object Object { get; set; }
    }

    public enum OPERATION
    { 
        Login,
        ReadMunicipalities,
        ReadPrograms,
        CreateFitnessCenter,
        ReadFitnessCenters,
        DeleteFitnesCenter,
        RegisterUser,
        ReserveProgram,
        ReadProducts,
        CreatePurchaseOrder,
        UpdatePurchaseOrder,
        ReadPurchaseOrder,
        PayPurchasePrice,
        FindUserTerms,
        CancellationOfReservedTerm
    }
}
