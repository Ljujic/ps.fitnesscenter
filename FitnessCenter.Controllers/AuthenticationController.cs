﻿using FitnessCenter.Controllers.Entities;
using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities.AuthenticationSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers
{
    public class AuthenticationController : IAuthenticationController
    {
        private readonly IUserAuthenticate _authenticate;
        private readonly IGeneratingHashPassword _generatingHash;

        public AuthenticationController(IUserAuthenticate authenticate, IGeneratingHashPassword generatingHash)
        {
            this._authenticate = authenticate;
            this._generatingHash = generatingHash;
        }

        public async Task<UserEntity> Login(string username, string password)
        {
            var hashedPassword = _generatingHash.HashPassword(password);
            UserEntity user = await _authenticate.Authenticate(username, hashedPassword);

            return user;
        }
    }
}
