﻿using FitnessCenter.Controllers.Entities;
using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using FitnessCenter.SystemOperationsEntities.AuthenticationSO;
using FitnessCenter.SystemOperationsEntities.FitnessCenterSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers
{
    public class FitnessCentersController : IFitnessCentersController
    {
        private readonly IFitnessCenterFacade _fitnessCenterFacade;
        private readonly IGeneratingHashPassword _generatingHash;

        public FitnessCentersController(IFitnessCenterFacade fitnessCenterFacade, IGeneratingHashPassword generatingHash) 
        {
            this._fitnessCenterFacade = fitnessCenterFacade;
            this._generatingHash = generatingHash;
        }

        public async Task<bool> CreateFitnessCenterAsync(FitnessCenterEntity fitnessCenter)
        {

            foreach (var trainer in fitnessCenter.Trainers)
            {
                trainer.Password = _generatingHash.HashPassword(trainer.Password);
            }
            return await _fitnessCenterFacade.CreateFitnessCenterAsync(fitnessCenter);
        }

        public async Task<bool> DeleteFitnessCenterAsync(long fitnessCenterId)
        {
            return await _fitnessCenterFacade.DeleteFitnessCenterAsync(fitnessCenterId);
        }

        public async Task<List<FitnessCenterEntity>> ReadAllAsync()
        {
            return await _fitnessCenterFacade.ReadAllAsync();
        }
    }
}
