﻿using FitnessCenter.Controllers.Entities;
using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities.TermSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers
{
    public class TermsController : ITermsController
    {
        private readonly ICancellationOfReservedTermSO _cancellationOfReservedTermSO;
        private readonly IFindUserTermsSO _findUserTermsSO;
        private readonly IReserveProgramTermSO _reserveProgramTermSO;

        public TermsController(ICancellationOfReservedTermSO cancellationOfReservedTermSO, IFindUserTermsSO findUserTermsSO, IReserveProgramTermSO reserveProgramTermSO)
        {
            this._cancellationOfReservedTermSO = cancellationOfReservedTermSO;
            this._findUserTermsSO = findUserTermsSO;
            this._reserveProgramTermSO = reserveProgramTermSO;
        }

        public async Task<bool> CancellationOfReservedTermAsync(long termId)
        {
            return await _cancellationOfReservedTermSO.CancellationOfReservedTermAsync(termId);
        }

        public async Task<List<TermEntity>> FindUserTermsAsync(long userId, DateTime startDate, DateTime endDate)
        {
            return await _findUserTermsSO.FindUserTermsAsync(userId, startDate, endDate);
        }

        public async Task<bool> ReserveProgramTermAsync(TermEntity term)
        {
            return await _reserveProgramTermSO.ReserveProgramTermAsync(term);
        }
    }
}
