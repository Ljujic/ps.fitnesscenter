﻿using FitnessCenter.Controllers.Entities;
using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers
{
    public class ProgramsController : IProgramsController
    {
        private readonly IReadAllSO<ProgramEntity> _readAllSO;

        public ProgramsController(IReadAllSO<ProgramEntity> readAllSO)
        {
            this._readAllSO = readAllSO;
        }

        public async Task<List<ProgramEntity>> ReadAllAsync()
        {
            return await _readAllSO.ReadAllAsync();
        }
    }
}
