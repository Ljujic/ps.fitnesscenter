﻿using FitnessCenter.Controllers.Entities;
using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers
{
    public class MunicipalitiesController : IMunicipalitiesController
    {
        private readonly IReadAllSO<MunicipalityEntity> _readAllSO;

        public MunicipalitiesController(IReadAllSO<MunicipalityEntity> readAllSO)
        {
            this._readAllSO = readAllSO;
        }

        public async Task<List<MunicipalityEntity>> ReadAllAsync()
        {
            return await _readAllSO.ReadAllAsync();
        }
    }
}
