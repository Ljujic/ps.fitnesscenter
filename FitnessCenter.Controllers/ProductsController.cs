﻿using FitnessCenter.Controllers.Entities;
using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers
{
    public class ProductsController : IProductsController
    {
        private readonly IReadAllSO<ProductEntity> _readAllSO;

        public ProductsController(IReadAllSO<ProductEntity> readAllSO)
        {
            this._readAllSO = readAllSO;
        }

        public async Task<List<ProductEntity>> ReadAllAsync()
        {
            return await _readAllSO.ReadAllAsync();
        }
    }
}
