﻿using FitnessCenter.Controllers.Entities;
using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities.AuthenticationSO;
using FitnessCenter.SystemOperationsEntities.UserSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers
{
    public class UsersController : IUsersController
    {
        private readonly IPayPurchaseOrderSO _payPurchaseOrderSO;
        private readonly IRegisterUserSO _registerUserSO;
        private readonly IGeneratingHashPassword _generatingHash;

        public UsersController(IPayPurchaseOrderSO payPurchaseOrderSO, IRegisterUserSO registerUserSO, IGeneratingHashPassword generatingHash)
        {
            this._payPurchaseOrderSO = payPurchaseOrderSO;
            this._registerUserSO = registerUserSO;
            this._generatingHash = generatingHash;
        }

        public async Task<bool> PayPurchaseOrderAsync(PurchaseOrderEntity purchaseOrder)
        {
            return await _payPurchaseOrderSO.PayPurchaseOrderAsync(purchaseOrder);
        }

        public async Task<bool> RegisterUserAsync(UserEntity user)
        {
            user.Password = _generatingHash.HashPassword(user.Password);
            return await _registerUserSO.RegisterUserAsync(user);
        }
    }
}
