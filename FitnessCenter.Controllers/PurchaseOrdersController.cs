﻿using FitnessCenter.Controllers.Entities;
using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities.PurchaseOrderSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers
{
    public class PurchaseOrdersController : IPurchaseOrdersController
    {
        private readonly IReadUserPurchaseOrdersSO _readUserPurchaseOrdersSO;
        private readonly IInsertUserPurchaseOrderSO _insertUserPurchaseOrderSO;
        private readonly IUpdatePurchaseOrderSO _updatePurchaseOrderSO;

        public PurchaseOrdersController(IReadUserPurchaseOrdersSO readUserPurchaseOrdersSO, IInsertUserPurchaseOrderSO insertUserPurchaseOrderSO, IUpdatePurchaseOrderSO updatePurchaseOrderSO)
        {
            this._readUserPurchaseOrdersSO = readUserPurchaseOrdersSO;
            this._insertUserPurchaseOrderSO = insertUserPurchaseOrderSO;
            this._updatePurchaseOrderSO = updatePurchaseOrderSO;
        }

        public async Task<bool> InsertPurchaseOrderAsync(PurchaseOrderEntity purchaseOrder)
        {
            return await _insertUserPurchaseOrderSO.InsertUserPurchaseOrder(purchaseOrder);
        }

        public async Task<List<PurchaseOrderEntity>> ReadUserPurchaseOrdersAsync(long userId)
        {
            return await _readUserPurchaseOrdersSO.ReadPurchaseOrdersAsync(userId);
        }

        public async Task<bool> UpdatePurchaseOrderAsync(PurchaseOrderEntity purchaseOrder)
        {
            return await _updatePurchaseOrderSO.UpdatePurchaseOrderAsync(purchaseOrder);
        }
    }
}
