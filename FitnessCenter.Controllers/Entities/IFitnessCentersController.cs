﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers.Entities
{
    public interface IFitnessCentersController
    {
        Task<List<FitnessCenterEntity>> ReadAllAsync();
        Task<bool> CreateFitnessCenterAsync(FitnessCenterEntity fitnessCenter);
        Task<bool> DeleteFitnessCenterAsync(long fitnessCenterId);
    }
}
