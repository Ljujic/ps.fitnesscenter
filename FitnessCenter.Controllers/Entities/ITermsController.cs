﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers.Entities
{
    public interface ITermsController
    {
        Task<List<TermEntity>> FindUserTermsAsync(long userId, DateTime startDate, DateTime endDate);
        Task<bool> CancellationOfReservedTermAsync(long termId);
        Task<bool> ReserveProgramTermAsync(TermEntity term);
    }
}
