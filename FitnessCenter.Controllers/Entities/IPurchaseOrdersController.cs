﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers.Entities
{
    public interface IPurchaseOrdersController
    {
        Task<List<PurchaseOrderEntity>> ReadUserPurchaseOrdersAsync(long userId);
        Task<bool> UpdatePurchaseOrderAsync(PurchaseOrderEntity purchaseOrder);
        Task<bool> InsertPurchaseOrderAsync(PurchaseOrderEntity purchaseOrder);
    }
}
