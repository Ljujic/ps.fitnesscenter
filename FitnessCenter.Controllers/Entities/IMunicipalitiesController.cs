﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Controllers.Entities
{
    public interface IMunicipalitiesController
    {
        Task<List<MunicipalityEntity>> ReadAllAsync();
    }
}
