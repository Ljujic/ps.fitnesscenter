﻿using FitnessCenter.Forms.UserInterfaceControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmReserveProgram : Form
    {
        private readonly ReserveProgramController _reserveProgramController;

        public FrmReserveProgram()
        {
            InitializeComponent();

            _reserveProgramController = new ReserveProgramController();
            ArrangeForm();
        }

        private void ArrangeForm()
        {
            try
            {
                if (Communication.Instance.ConnectWithServer())
                {
                    cmbFitnessCenterPrograms.DataSource = Communication.Instance.ReadPrograms();
                    cmbFitnessCenterPrograms.DisplayMember = "Name";
                }
                else
                {
                    MessageBox.Show("Server is not available! Try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _reserveProgramController.ReserveProgram(cmbFitnessCenterPrograms, txtStartDate, Session.Instance.User);
        }
    }
}
