﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmRegisterUser : Form
    {
        public FrmRegisterUser()
        {
            InitializeComponent();
            ArrangeForm();
        }

        private void ArrangeForm()
        {
            try
            {
                if (Communication.Instance.ConnectWithServer())
                {
                    cmbFitnessCenters.DataSource = Communication.Instance.ReadFitnessCenters();
                    cmbFitnessCenters.DisplayMember = "Name";
                }
                else
                {
                    MessageBox.Show("Server is not available! Try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                if (Communication.Instance.ConnectWithServer())
                {
                    if (FieldsValidation(txtNameSurname.Text, txtUsername.Text, txtPassword.Text, txtEmail.Text, txtAddress.Text, txtDateOfBirth.Text, txtBankCardBalance.Text))
                    {
                        string nameSurname = txtNameSurname.Text;
                        string username = txtUsername.Text;
                        string password = txtPassword.Text;
                        string email = txtEmail.Text;
                        string address = txtAddress.Text;
                        DateTime dateOfBirth = DateTime.Parse(txtDateOfBirth.Text);
                        double cardBalance = Double.Parse(txtBankCardBalance.Text);
                        FitnessCenterEntity fitnessCenter = (FitnessCenterEntity)cmbFitnessCenters.SelectedItem;


                        bool isRegistered = Communication.Instance.Register(nameSurname, username, password, email, address, dateOfBirth, cardBalance, fitnessCenter.Id);

                        if (isRegistered)
                        {
                            MessageBox.Show($"Successfully registered!");
                            ClearFormFields(txtNameSurname, txtUsername, txtPassword, txtEmail, txtAddress, txtDateOfBirth, txtBankCardBalance);

                            FrmLogin frmLogin = new FrmLogin();
                            this.Close();
                            frmLogin.Show();
                        }
                        else
                            MessageBox.Show("Failed registration!");
                    }
                }
                else
                {
                    MessageBox.Show("Server is not available! Try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearFormFields(TextBox txtNameSurname, TextBox txtUsername, TextBox txtPassword, TextBox txtEmail, TextBox txtAddress, TextBox txtDateOfBirth, TextBox txtBankCardBalance)
        {
            txtNameSurname.Clear();
            txtUsername.Clear();
            txtPassword.Clear();
            txtEmail.Clear();
            txtAddress.Clear();
            txtDateOfBirth.Clear();
            txtBankCardBalance.Clear();
        }

        private bool FieldsValidation(string nameSurname, string username, string password, string email, string address, string dateOfBirth, string bankCardBalance)
        {
            bool isValid = true;

            if (String.IsNullOrEmpty(nameSurname))
            {
                txtNameSurname.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtNameSurname.BackColor = Color.White;

            if (String.IsNullOrEmpty(username))
            {
                txtUsername.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtUsername.BackColor = Color.White;

            if (String.IsNullOrEmpty(password) || password.Length < 6)
            {
                txtPassword.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtPassword.BackColor = Color.White;

            if (String.IsNullOrEmpty(email))
            {
                txtEmail.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtEmail.BackColor = Color.White;

            if (String.IsNullOrEmpty(address))
            {
                txtAddress.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtAddress.BackColor = Color.White;

            if (!double.TryParse(bankCardBalance, out double _))
            {
                txtBankCardBalance.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtBankCardBalance.BackColor = Color.White;

            DateTime date = DateTime.Parse(txtDateOfBirth.Text);
            if (!DateTime.TryParseExact(dateOfBirth, "MM.dd.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime) ||
                 (date.Year > DateTime.Now.Year) ||
                (date.Year == DateTime.Now.Year && date.Month >= DateTime.Now.Month) ||
                (date.Year == DateTime.Now.Year && date.Month == DateTime.Now.Month && date.Day >= DateTime.Now.Day))
            {
                txtDateOfBirth.BackColor = Color.Red;
                MessageBox.Show("Error! Date format: MM.dd.yyyy");
                isValid = false;
            }
            else
                txtDateOfBirth.BackColor = Color.White;

            return isValid;
        }
    }
}
