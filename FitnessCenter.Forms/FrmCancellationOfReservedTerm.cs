﻿using FitnessCenter.Domain;
using FitnessCenter.Forms.UserInterfaceControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmCancellationOfReservedTerm : Form
    {
        private readonly CancellationOfResevedTermController _cancellationOfResevedTermController;

        public FrmCancellationOfReservedTerm()
        {
            InitializeComponent();

            _cancellationOfResevedTermController = new CancellationOfResevedTermController();
        }

        private void btnFindTerms_Click(object sender, EventArgs e)
        {
            try
            {
                if (Communication.Instance.ConnectWithServer())
                {
                    List<TermEntity> terms = _cancellationOfResevedTermController.FindUserTerms(Session.Instance.User, txtStartDate, txtEndDate);

                    if (terms == null || terms.Count == 0)
                    {
                        MessageBox.Show("System can not find terms!");
                    }
                    else
                    { 
                        dgvTerms.DataSource = terms;
                        dgvTerms.Columns["Id"].Visible = false;
                        dgvTerms.Columns["ProgramId"].Visible = false;
                        dgvTerms.Columns["UserId"].Visible = false;
                        dgvTerms.Columns["EndDate"].Visible = false;
                        dgvTerms.Columns["Program"].Visible = false;
                    }
                }
                else
                {
                    MessageBox.Show("Server is not available! Try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelTerm_Click(object sender, EventArgs e)
        {
            if (dgvTerms.SelectedRows.Count > 0)
            {
                TermEntity term = ((TermEntity)dgvTerms.SelectedRows[0].DataBoundItem);

                bool isCancelledTerm = _cancellationOfResevedTermController.CancellationOfReservedTerm(term.Id);

                if (isCancelledTerm)
                { 
                    MessageBox.Show("Term is cancelled successfully!");

                    List<TermEntity> terms = _cancellationOfResevedTermController.FindUserTerms(Session.Instance.User, txtStartDate, txtEndDate);
                    dgvTerms.DataSource = terms;
                }
                else
                    MessageBox.Show("Error! System can not cancel selected term!");
            }
        }
    }
}
