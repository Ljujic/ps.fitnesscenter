﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnRegisterHere_Click(object sender, EventArgs e)
        {
            FrmRegisterUser frmRegisterUser = new FrmRegisterUser();

            this.Hide();
            frmRegisterUser.Show();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (Communication.Instance.ConnectWithServer())
                {
                    if (FieldsValidation(txtUsername.Text, txtPassword.Text))
                    {
                        string username = txtUsername.Text;
                        string password = txtPassword.Text;

                        UserEntity user = Communication.Instance.Login(username, password);

                        if (user != null)
                        {
                            Session.Instance.User = user;
                            txtUsername.Clear();
                            txtPassword.Clear();

                            if (user.Username == "admin")
                            {
                                MessageBox.Show($"ADMIN '{user.NameSurname}' LOGGED!");
                                FrmFitnessCentersIndex frmFitnessCentersIndex = new FrmFitnessCentersIndex();
                                frmFitnessCentersIndex.Show();
                                this.Hide();
                            }
                            else
                            {
                                MessageBox.Show($"Welcome {user.NameSurname}");
                                FrmUserIndex frmUserIndex = new FrmUserIndex();
                                frmUserIndex.Show();
                                this.Hide();
                            }
                        }
                        else
                            MessageBox.Show("Wrong username or password! Try again!");
                    }                    
                }
                else
                {
                    MessageBox.Show("Server is not available! Try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private bool FieldsValidation(string username, string password)
        {
            bool isValid = true;

            if (String.IsNullOrEmpty(username))
            {
                txtUsername.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtUsername.BackColor = Color.White;

            if (String.IsNullOrEmpty(password) || password.Length < 6)
            {
                txtPassword.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtPassword.BackColor = Color.White;

            return isValid;
        }
    }
}
