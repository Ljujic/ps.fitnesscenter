﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms.UserInterfaceControllers
{
    public class ReserveProgramController
    {
        public ReserveProgramController()
        {

        }

        public void ReserveProgram(ComboBox cmbProgram, TextBox txtDate, UserEntity user)
        {
            try
            {
                if(FieldsValidation(cmbProgram, txtDate))
                {
                    ProgramEntity program = (ProgramEntity)cmbProgram.SelectedItem;
                    TermEntity term = new TermEntity { ProgramId=program.Id, UserId=user.Id, DateOfTerm=DateTime.Parse(txtDate.Text) };

                    bool isReserved = Communication.Instance.ReserveProgram(term);

                    if (isReserved)
                    {
                        MessageBox.Show("Term reserved successfully!");
                        ClearFormFields(cmbProgram, txtDate);
                    }
                    else
                        MessageBox.Show("Term is not reserved! Try again!");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("System can not reserve term!");
            }
        }

        private void ClearFormFields(ComboBox cmbProgram, TextBox txtDate)
        {
            txtDate.Clear();
            cmbProgram.SelectedIndex = 0;
        }

        private bool FieldsValidation(ComboBox cmbProgram, TextBox txtDate)
        {
            bool isValid = true;

            if (cmbProgram.SelectedItem == null)
            {
                isValid = false;
                cmbProgram.BackColor = Color.Red;
            }
            else
                cmbProgram.BackColor = Color.White;

            DateTime date = DateTime.Parse(txtDate.Text);
            if (!DateTime.TryParseExact(txtDate.Text, "MM.dd.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime) ||
                (date.Year < DateTime.Now.Year) ||
                (date.Year == DateTime.Now.Year && date.Month < DateTime.Now.Month) ||
                (date.Year == DateTime.Now.Year && date.Month == DateTime.Now.Month && date.Day < DateTime.Now.Day))
            {
                isValid = false;
                MessageBox.Show("Error! Date format: MM.dd.yyyy");
                txtDate.BackColor = Color.Red;
            }
            else
                txtDate.BackColor = Color.White;

            return isValid;
        }
    }
}
