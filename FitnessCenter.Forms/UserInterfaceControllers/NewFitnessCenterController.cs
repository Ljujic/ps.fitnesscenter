﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms.UserInterfaceControllers
{
    public class NewFitnessCenterController
    {
        private List<ProductEntity> _products;
        private List<TrainerEntity> _trainers; 

        public NewFitnessCenterController()
        {
            _products = new List<ProductEntity>();
            _trainers = new List<TrainerEntity>();
        }

        public void AddFitnessCenterProduct(ProductEntity products)
        {
            _products.Add(products);
        }

        public bool AddFitnessCenterTrainer(TrainerEntity trainer)
        {
            foreach (var tr in _trainers)
            {
                if ((tr.Username == trainer.Username) || (tr.Email == trainer.Email))
                { 
                    MessageBox.Show("Username/email already used! Please change!");
                    return false;                
                }
            }

            _trainers.Add(trainer);
            return true;
        }

        public void SaveFitnessCenter(ComboBox cmbMunicipality, TextBox txtName, TextBox txtAddress, TextBox txtType, TextBox txtPhoneNumber, TextBox txtDetails)
        {
            try
            {
                if (FieldsValidation(cmbMunicipality, txtAddress, txtName, txtType, txtPhoneNumber, txtDetails))
                {
                    FitnessCenterEntity fitnessCenter = new FitnessCenterEntity();

                    fitnessCenter.Name = txtName.Text;
                    fitnessCenter.Type = txtType.Text;
                    fitnessCenter.PhoneNumber = txtPhoneNumber.Text;
                    fitnessCenter.Details = txtDetails.Text;
                    fitnessCenter.Address = txtAddress.Text;
                    fitnessCenter.Trainers = _trainers;
                    fitnessCenter.Products = _products;
                    MunicipalityEntity municipality = (MunicipalityEntity)cmbMunicipality.SelectedItem;
                    fitnessCenter.MunicipalityId = municipality.Id;

                    bool isInserted = Communication.Instance.SaveFitnessCenter(fitnessCenter);

                    if (isInserted)
                    {
                        MessageBox.Show("Fitness center inserted successfully!");
                        ClearFormFields(cmbMunicipality, txtName, txtAddress, txtType, txtPhoneNumber, txtDetails);
                    }
                    else
                        MessageBox.Show("Fitness center is not inserted! Try again!");
                }
                else
                    MessageBox.Show("System can not save fitness center!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error! Try again!");
            }
        }

        private bool FieldsValidation(ComboBox cmbMunicipality, TextBox txtName, TextBox txtAddress, TextBox txtType, TextBox txtPhoneNumber, TextBox txtDetails)
        {
            var isValid = true;

            if (String.IsNullOrEmpty(txtName.Text))
            {
                txtName.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtName.BackColor = Color.White;

            if (String.IsNullOrEmpty(txtType.Text))
            {
                txtType.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtType.BackColor = Color.White;

            if (String.IsNullOrEmpty(txtDetails.Text))
            {
                txtDetails.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtDetails.BackColor = Color.White;

            if (String.IsNullOrEmpty(txtPhoneNumber.Text))
            {
                txtPhoneNumber.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtPhoneNumber.BackColor = Color.White;

            if (String.IsNullOrEmpty(txtAddress.Text))
            {
                txtAddress.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtAddress.BackColor = Color.White;

            if (cmbMunicipality.SelectedItem == null)
            {
                isValid = false;
                cmbMunicipality.BackColor = Color.Red;
            }
            else
                cmbMunicipality.BackColor = Color.White;

            return isValid;
        }

        private void ClearFormFields(ComboBox cmbMunicipality, TextBox txtName, TextBox txtAddress, TextBox txtType, TextBox txtPhoneNumber, TextBox txtDetails)
        {
            cmbMunicipality.SelectedIndex = 0;
            txtName.Clear();
            txtAddress.Clear();
            txtType.Clear();
            txtPhoneNumber.Clear();
            txtDetails.Clear();
        }
    }
}
