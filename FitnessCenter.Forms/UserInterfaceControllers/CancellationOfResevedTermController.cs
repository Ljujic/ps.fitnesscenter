﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms.UserInterfaceControllers
{
    public class CancellationOfResevedTermController
    {
        public CancellationOfResevedTermController()
        {

        }

        public List<TermEntity> FindUserTerms(UserEntity user, TextBox startDate, TextBox endDate)
        {
            if (FieldsValidation(startDate, endDate))
            {
                return Communication.Instance.FindTerms(user, DateTime.Parse(startDate.Text), DateTime.Parse(endDate.Text));
            }

            return null;
        }

        private bool FieldsValidation(TextBox txtStartDate, TextBox txtEndDate)
        {
            bool isValid = true;

            DateTime startDate = DateTime.Parse(txtStartDate.Text);
            if (!DateTime.TryParseExact(txtStartDate.Text, "MM.dd.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime startDateTime) ||
                 (startDate.Year < DateTime.Now.Year) ||
                (startDate.Year == DateTime.Now.Year && startDate.Month < DateTime.Now.Month) ||
                (startDate.Year == DateTime.Now.Year && startDate.Month == DateTime.Now.Month && startDate.Day < DateTime.Now.Day))
            {
                txtStartDate.BackColor = Color.Red;
                MessageBox.Show("Error! Date format: MM.dd.yyyy");
                isValid = false;
            }
            else
                txtStartDate.BackColor = Color.White;

            DateTime endDate = DateTime.Parse(txtEndDate.Text);
            if (!DateTime.TryParseExact(txtEndDate.Text, "MM.dd.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime endDateTime))
            {
                txtEndDate.BackColor = Color.Red;
                MessageBox.Show("Error! Date format: MM.dd.yyyy");
                isValid = false;
            }
            else
                txtStartDate.BackColor = Color.White;

            return isValid;
        }

        public bool CancellationOfReservedTerm(long termId)
        {
            return Communication.Instance.CancellationOfReservedTerm(termId);
        }
    }
}
