﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Forms.UserInterfaceControllers
{
    public class PayPurchaseOrderController
    {
        public PayPurchaseOrderController()
        {

        }

        public bool PayPurchaseOrder(PurchaseOrderEntity purchaseOrder)
        {
            return Communication.Instance.PayPurchaseOrder(purchaseOrder);
        }
    }
}
