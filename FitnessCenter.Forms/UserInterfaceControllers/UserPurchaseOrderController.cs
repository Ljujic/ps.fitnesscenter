﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms.UserInterfaceControllers
{
    public class UserPurchaseOrderController
    {

        public UserPurchaseOrderController()
        {
        }

        public List<PurchaseOrderEntity> ReadUserPurchaseOrder(UserEntity user)
        {
            return Communication.Instance.ReadUserPurchaseOrder(user);
        }

        public void CreatePurchaseOrder(UserEntity user, BindingList<PurchaseOrderItemEntity> purchaseOrderItems, TextBox txtTotalPrice)
        {
            if (Validation(txtTotalPrice))
            { 
                PurchaseOrderEntity purchaseOrder = new PurchaseOrderEntity();
                purchaseOrder.UserId = user.Id;
                purchaseOrder.FitnessCenterId = user.FitnessCenterId;
                purchaseOrder.TotalPrice = double.Parse(txtTotalPrice.Text);
                purchaseOrder.PurchaseOrderItems = purchaseOrderItems.ToList();
                purchaseOrder.DateOfDispatch = DateTime.Now.AddDays(20);

                bool isInserted = Communication.Instance.CreatePurchaseOrder(purchaseOrder);

                if (isInserted)
                {
                    MessageBox.Show("Purchase order created successfully!");
                    ClearFormFields(txtTotalPrice, purchaseOrderItems);
                }
                else
                    MessageBox.Show("Purchase order is not created! Try again!");
            }
        }

        private void ClearFormFields(TextBox txtTotalPrice, BindingList<PurchaseOrderItemEntity> purchaseOrderItems)
        {
            txtTotalPrice.Clear();
            purchaseOrderItems.Clear();
        }

        private bool Validation(TextBox txtTotalPrice)
        {
            bool isValid = true;

            if (!double.TryParse(txtTotalPrice.Text, out double _))
            {
                txtTotalPrice.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtTotalPrice.BackColor = Color.White;


            return isValid;
        }

        internal List<ProductEntity> ReadProducts()
        {
            return Communication.Instance.ReadProducts();
        }
    }
}
