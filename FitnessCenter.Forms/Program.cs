﻿using FitnessCenter.Broker;
using FitnessCenter.Broker.Repository;
using FitnessCenter.Domain;
using FitnessCenter.SystemOperations;
using FitnessCenter.SystemOperations.FitnessCenterSO;
using FitnessCenter.SystemOperations.PurchaseOrderSO;
using FitnessCenter.SystemOperations.TermSO.ReserveProgramTermSO;
using FitnessCenter.SystemOperations.TermSO;
using FitnessCenter.SystemOperationsEntities;
using FitnessCenter.SystemOperationsEntities.FitnessCenterSO;
using FitnessCenter.SystemOperationsEntities.PurchaseOrderSO;
using FitnessCenter.SystemOperationsEntities.TermSO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessCenter.SystemOperationsEntities.UserSO;
using FitnessCenter.SystemOperations.UserSO;

namespace FitnessCenter.Forms
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmLogin());
        }
    }
}
