﻿namespace FitnessCenter.Forms
{
    partial class FrmUserIndex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFitnessProgramReservation = new System.Windows.Forms.Button();
            this.btnCreatePurchaseOrder = new System.Windows.Forms.Button();
            this.btnCancellationTerms = new System.Windows.Forms.Button();
            this.btnPayPurchaseOrder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFitnessProgramReservation
            // 
            this.btnFitnessProgramReservation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnFitnessProgramReservation.BackColor = System.Drawing.SystemColors.ControlText;
            this.btnFitnessProgramReservation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFitnessProgramReservation.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnFitnessProgramReservation.Location = new System.Drawing.Point(244, 76);
            this.btnFitnessProgramReservation.Name = "btnFitnessProgramReservation";
            this.btnFitnessProgramReservation.Size = new System.Drawing.Size(349, 42);
            this.btnFitnessProgramReservation.TabIndex = 0;
            this.btnFitnessProgramReservation.Text = "Fitness program reservation";
            this.btnFitnessProgramReservation.UseVisualStyleBackColor = false;
            this.btnFitnessProgramReservation.Click += new System.EventHandler(this.btnFitnessProgramReservation_Click);
            // 
            // btnCreatePurchaseOrder
            // 
            this.btnCreatePurchaseOrder.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCreatePurchaseOrder.BackColor = System.Drawing.SystemColors.ControlText;
            this.btnCreatePurchaseOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreatePurchaseOrder.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCreatePurchaseOrder.Location = new System.Drawing.Point(244, 124);
            this.btnCreatePurchaseOrder.Name = "btnCreatePurchaseOrder";
            this.btnCreatePurchaseOrder.Size = new System.Drawing.Size(349, 44);
            this.btnCreatePurchaseOrder.TabIndex = 1;
            this.btnCreatePurchaseOrder.Text = "Create purchase order";
            this.btnCreatePurchaseOrder.UseVisualStyleBackColor = false;
            this.btnCreatePurchaseOrder.Click += new System.EventHandler(this.btnCreatePurchaseOrder_Click);
            // 
            // btnCancellationTerms
            // 
            this.btnCancellationTerms.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCancellationTerms.BackColor = System.Drawing.SystemColors.ControlText;
            this.btnCancellationTerms.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancellationTerms.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancellationTerms.Location = new System.Drawing.Point(244, 174);
            this.btnCancellationTerms.Name = "btnCancellationTerms";
            this.btnCancellationTerms.Size = new System.Drawing.Size(349, 44);
            this.btnCancellationTerms.TabIndex = 2;
            this.btnCancellationTerms.Text = "Cancellation of reserved terms";
            this.btnCancellationTerms.UseVisualStyleBackColor = false;
            this.btnCancellationTerms.Click += new System.EventHandler(this.btnCancellationTerms_Click);
            // 
            // btnPayPurchaseOrder
            // 
            this.btnPayPurchaseOrder.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnPayPurchaseOrder.BackColor = System.Drawing.SystemColors.ControlText;
            this.btnPayPurchaseOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPayPurchaseOrder.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPayPurchaseOrder.Location = new System.Drawing.Point(244, 224);
            this.btnPayPurchaseOrder.Name = "btnPayPurchaseOrder";
            this.btnPayPurchaseOrder.Size = new System.Drawing.Size(349, 44);
            this.btnPayPurchaseOrder.TabIndex = 3;
            this.btnPayPurchaseOrder.Text = "Pay purchase order";
            this.btnPayPurchaseOrder.UseVisualStyleBackColor = false;
            this.btnPayPurchaseOrder.Click += new System.EventHandler(this.btnPayPurchaseOrder_Click);
            // 
            // FrmUserIndex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(855, 417);
            this.Controls.Add(this.btnPayPurchaseOrder);
            this.Controls.Add(this.btnCancellationTerms);
            this.Controls.Add(this.btnCreatePurchaseOrder);
            this.Controls.Add(this.btnFitnessProgramReservation);
            this.Name = "FrmUserIndex";
            this.Text = "User Index";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFitnessProgramReservation;
        private System.Windows.Forms.Button btnCreatePurchaseOrder;
        private System.Windows.Forms.Button btnCancellationTerms;
        private System.Windows.Forms.Button btnPayPurchaseOrder;
    }
}