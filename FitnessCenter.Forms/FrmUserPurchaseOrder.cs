﻿using FitnessCenter.Domain;
using FitnessCenter.Forms.UserInterfaceControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmUserPurchaseOrder : Form
    {
        private readonly UserPurchaseOrderController _userPurchaseOrderController;
        private BindingList<PurchaseOrderItemEntity> _orderItems;

        public FrmUserPurchaseOrder()
        {
            InitializeComponent();
            _orderItems = new BindingList<PurchaseOrderItemEntity>();
            _userPurchaseOrderController = new UserPurchaseOrderController();
            ArrangeForm();
        }

        private void ArrangeForm()
        {
            try
            {
                if (Communication.Instance.ConnectWithServer())
                {
                    cmbProducts.DataSource = _userPurchaseOrderController.ReadProducts();
                    cmbProducts.DisplayMember = "Name";

                    dgvOrderItems.DataSource = _orderItems;
                    dgvOrderItems.Columns["Id"].Visible = false;
                    dgvOrderItems.Columns["PurchaseOrderId"].Visible = false;
                }
                else
                {
                    MessageBox.Show("Server is not available! Try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            if (int.TryParse(txtProductAmount.Text, out int _))
            {
                txtProductAmount.BackColor = Color.White;
                ProductEntity product = (ProductEntity)cmbProducts.SelectedItem;                

                bool isProductAvailable = AvailableProductQuantity(product, int.Parse(txtProductAmount.Text));

                if (isProductAvailable)
                {
                    PurchaseOrderItemEntity orderItem = new PurchaseOrderItemEntity { ProductId = product.Id, Amount = int.Parse(txtProductAmount.Text) };
                    orderItem.Price = product.Price * orderItem.Amount;

                    if (!_orderItems.Contains(orderItem))
                    {
                        _orderItems.Add(orderItem);
                        txtProductAmount.Clear();
                    }
                    else
                    {
                        MessageBox.Show("Product already added!");
                    }

                    double totalPrice = 0;
                    foreach (var item in _orderItems)
                    {
                        totalPrice += item.Price;
                        txtTotalPrice.Text = totalPrice.ToString();
                    }
                }
                else
                    MessageBox.Show("We are unable to process your request! Please, try again!");
                
            }
            else
            { 
                MessageBox.Show("You must set amount!");
                txtProductAmount.BackColor = Color.Red;
            }
        }

        private bool AvailableProductQuantity(ProductEntity product, int amount)
        {
            int sumAmount = 0;
            foreach (var poItem in _orderItems)
            {
                if (poItem.ProductId == product.Id)
                    sumAmount += poItem.Amount;
            }
            sumAmount += amount;

            if (sumAmount > product.Quantity)
                return false;
            return true;
        }

        private void btnDeleteProductFromOrder_Click(object sender, EventArgs e)
        {
            if (dgvOrderItems.SelectedRows.Count > 0)
            {
                _orderItems.Remove((PurchaseOrderItemEntity)dgvOrderItems.SelectedRows[0].DataBoundItem);

                double totalPrice = 0;
                foreach (var item in _orderItems)
                {
                    totalPrice += item.Price;
                    txtTotalPrice.Text = totalPrice.ToString();
                }
            }
        }

        private void btnCreatePurchaseOrder_Click(object sender, EventArgs e)
        {
            _userPurchaseOrderController.CreatePurchaseOrder(Session.Instance.User, _orderItems, txtTotalPrice);
        }
    }
}
