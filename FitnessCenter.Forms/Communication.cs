﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Forms
{
    public class Communication
    {
        private static Communication _instance;
        private Socket _clientSocket;
        private NetworkStream _stream;        

        private BinaryFormatter _formatter = new BinaryFormatter();

        internal void SendRequest(Request request)
        {
            _formatter.Serialize(_stream, request);
        }        

        internal Response ReadResponse()
        {
            try
            {
                return (Response)_formatter.Deserialize(_stream);
            }
            catch (Exception e)
            {
                Debug.WriteLine(">>>" + e.Message);
                return null;
            }
        }        

        private Communication()
        {
        }
        public static Communication Instance
        {
            get
            {
                if (_instance == null) _instance = new Communication();
                return _instance;
            }
        }        

        public bool ConnectWithServer()
        {
            try
            {
                if (_clientSocket == null || !_clientSocket.Connected)
                {
                    _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _clientSocket.Connect("127.0.0.1", 9090);
                    _stream = new NetworkStream(_clientSocket);
                }
                return true;
            }
            catch (SocketException)
            {
                return false;
            }
        }

        //SO 1
        internal bool Register(string nameSurname, string username, string password, string email, string address, DateTime dateOfBirth, double cardBalance, long fitnessCenterId)
        {
            UserEntity user = new UserEntity { NameSurname=nameSurname, Username = username, Password = password, Email = email, Address = address, DateOfBirth = dateOfBirth, BankCardBalance = cardBalance, FitnessCenterId=fitnessCenterId };

            Request request = new Request { Operation = OPERATION.RegisterUser, Object = user };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (bool)response.Object;

            throw new Exception(response.Message);
        }

        //SO 2
        internal UserEntity Login(string username, string password)
        {
            UserEntity user = new UserEntity { Username=username, Password=password };

            Request request = new Request { Operation=OPERATION.Login, Object=user };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if(response.Signal == SIGNAL.Ok)
                return (UserEntity)response.Object;

            throw new Exception(response.Message);
        }

        //SO 3
        internal List<FitnessCenterEntity> ReadFitnessCenters()
        {
            Request request = new Request { Operation = OPERATION.ReadFitnessCenters };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (List<FitnessCenterEntity>)response.Object;
            else
                throw new Exception(response.Message);
        }

        //SO 4
        internal List<MunicipalityEntity> ReadMunicipalities()
        {
            Request request = new Request { Operation=OPERATION.ReadMunicipalities };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (List<MunicipalityEntity>)response.Object;
            else
                throw new Exception(response.Message);
        }

        //SO 5
        internal List<ProgramEntity> ReadPrograms()
        {
            Request request = new Request { Operation = OPERATION.ReadPrograms };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (List<ProgramEntity>)response.Object;
            else
                throw new Exception(response.Message);
        }

        //SO 6
        internal bool SaveFitnessCenter(FitnessCenterEntity fitnessCenter)
        {
            Request request = new Request { Operation=OPERATION.CreateFitnessCenter, Object=fitnessCenter };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (bool)response.Object;
            else
                throw new Exception(response.Message);
        }

        //SO 7
        internal bool DeleteFitnessCenter(long id)
        {
            Request request = new Request { Operation=OPERATION.DeleteFitnesCenter, Object=id };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (bool)response.Object;
            else
                throw new Exception(response.Message);
        }

        //SO 8
        internal bool ReserveProgram(TermEntity term)
        {
            Request request = new Request { Operation = OPERATION.ReserveProgram, Object = term };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (bool)response.Object;
            else
                throw new Exception(response.Message);
        }

        //SO 9
        internal List<ProductEntity> ReadProducts()
        {
            Request request = new Request { Operation = OPERATION.ReadProducts };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (List<ProductEntity>)response.Object;
            else
                throw new Exception(response.Message);
        }

        //SO 10
        internal bool CreatePurchaseOrder(PurchaseOrderEntity purchaseOrder)
        {
            Request request = new Request { Operation = OPERATION.CreatePurchaseOrder, Object = purchaseOrder };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (bool)response.Object;
            else
                throw new Exception(response.Message);
        }

        //SO 11
        internal List<PurchaseOrderEntity> ReadUserPurchaseOrder(UserEntity user)
        {
            Request request = new Request { Object=user.Id, Operation=OPERATION.ReadPurchaseOrder };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (List<PurchaseOrderEntity>)response.Object;
            else
                throw new Exception(response.Message);
        }

        //SO 12
        internal bool PayPurchaseOrder(PurchaseOrderEntity purchaseOrder)
        {
            Request request = new Request { Object = purchaseOrder, Operation = OPERATION.PayPurchasePrice };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (bool)response.Object;
            else
                throw new Exception(response.Message);
        }

        internal List<TermEntity> FindTerms(UserEntity user, DateTime startDate, DateTime endDate)
        {
            TermEntity term = new TermEntity { DateOfTerm=startDate, EndDate=endDate, UserId=user.Id };
            Request request = new Request { Object = term, Operation = OPERATION.FindUserTerms };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (List<TermEntity>)response.Object;
            else
                throw new Exception(response.Message);
        }

        //SO 14
        internal bool CancellationOfReservedTerm(long termId)
        {
            Request request = new Request { Object = termId, Operation = OPERATION.CancellationOfReservedTerm };
            _formatter.Serialize(_stream, request);
            Response response = ReadResponse();

            if (response.Signal == SIGNAL.Ok)
                return (bool)response.Object;
            else
                throw new Exception(response.Message);
        }
    }
}