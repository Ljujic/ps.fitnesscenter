﻿using FitnessCenter.Domain;
using FitnessCenter.Forms.UserInterfaceControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmAddFitnessCenterTrainers : Form
    {
        private NewFitnessCenterController _controller;
        private BindingList<ProgramEntity> _programs;

        public FrmAddFitnessCenterTrainers(NewFitnessCenterController controller)
        {
            InitializeComponent();

            this._programs = new BindingList<ProgramEntity>();
            this._controller = controller;
            ArrangeForm();
        }

        private void ArrangeForm()
        {
            cmbPrograms.DataSource = Communication.Instance.ReadPrograms();
            cmbPrograms.DisplayMember = "Name";
            dgvTrainerPrograms.DataSource = _programs;
        }

        private void brnSaveTrainer_Click(object sender, EventArgs e)
        {
            try
            {
                if (FieldsValidation(txtNameSurname, txtEmail, txtUsername, txtPassword, txtAddress, txtReference))
                {
                    TrainerEntity trainer = new TrainerEntity();
                    trainer.NameSurname = txtNameSurname.Text;
                    trainer.Email = txtEmail.Text;
                    trainer.Username = txtUsername.Text;
                    trainer.Password = txtPassword.Text;
                    trainer.Address = txtAddress.Text;
                    trainer.Reference = txtReference.Text;
                    trainer.Programs = _programs.ToList();

                    bool isAdded = _controller.AddFitnessCenterTrainer(trainer);

                    if (isAdded)
                    { 
                        MessageBox.Show("Trainer added successfully!");
                        ClearFormFields(txtNameSurname, txtEmail, txtUsername, txtPassword, txtAddress, txtReference);                                    
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error!");
            }
        }

        private bool FieldsValidation(TextBox txtNameSurname, TextBox txtEmail, TextBox txtUsername, TextBox txtPassword, TextBox txtAddress, TextBox txtReference)
        {
            var isValid = true;

            if (String.IsNullOrEmpty(txtNameSurname.Text))
            {
                txtNameSurname.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtNameSurname.BackColor = Color.White;

            if (String.IsNullOrEmpty(txtEmail.Text))
            {
                txtEmail.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtEmail.BackColor = Color.White;

            if (String.IsNullOrEmpty(txtUsername.Text))
            {
                txtUsername.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtUsername.BackColor = Color.White;

            if (String.IsNullOrEmpty(txtPassword.Text) || txtPassword.Text.Length < 6)
            {
                txtPassword.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtPassword.BackColor = Color.White;

            if (String.IsNullOrEmpty(txtAddress.Text))
            {
                isValid = false;
                txtAddress.BackColor = Color.Red;
            }
            else
                txtAddress.BackColor = Color.White;

            if (String.IsNullOrEmpty(txtReference.Text))
            {
                isValid = false;
                txtReference.BackColor = Color.Red;
            }
            else
                txtReference.BackColor = Color.White;

            return isValid;
        }

        private void ClearFormFields(TextBox txtNameSurname, TextBox txtEmail, TextBox txtUsername, TextBox txtPassword, TextBox txtAddress, TextBox txtReference)
        {
            txtNameSurname.Clear();
            txtEmail.Clear();
            txtUsername.Clear();
            txtPassword.Clear();
            txtAddress.Clear();
            txtReference.Clear();
            _programs.Clear();
        }

        private void btnAddProgram_Click(object sender, EventArgs e)
        {
            ProgramEntity program = (ProgramEntity)cmbPrograms.SelectedItem;
            if (!_programs.Contains(program))
            {
                _programs.Add(program);
            }
            else
                MessageBox.Show("Program already added!");
        }

        private void btnRemoveProgram_Click(object sender, EventArgs e)
        {
            if (dgvTrainerPrograms.SelectedRows.Count > 0)
            {
                _programs.Remove((ProgramEntity)dgvTrainerPrograms.SelectedRows[0].DataBoundItem);
            }
        }
    }
}
