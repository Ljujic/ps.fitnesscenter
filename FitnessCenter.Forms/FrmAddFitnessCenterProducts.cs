﻿using FitnessCenter.Domain;
using FitnessCenter.Forms.UserInterfaceControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmAddFitnessCenterProducts : Form
    {
        private NewFitnessCenterController _controller;

        public FrmAddFitnessCenterProducts(NewFitnessCenterController controller)
        {
            InitializeComponent();
            this._controller = controller;
        }

        private void btnSaveProduct_Click(object sender, EventArgs e)
        {
            try
            {
                if (FieldsValidation(txtProductName, txtProductDetails, txtProductPrice, txtProductQuantity))
                {
                    ProductEntity product = new ProductEntity { Name=txtProductName.Text, Details=txtProductDetails.Text, Price=double.Parse(txtProductPrice.Text), Quantity=int.Parse(txtProductQuantity.Text) };

                    _controller.AddFitnessCenterProduct(product);
                    MessageBox.Show("Product added successfully!");
                    ClearFormFields(txtProductName, txtProductDetails, txtProductPrice, txtProductQuantity);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error!");
            }
        }

        private bool FieldsValidation(TextBox txtName, TextBox txtDetails, TextBox txtPrice, TextBox txtQuantity)
        {
            var isValid = true;

            if (String.IsNullOrEmpty(txtName.Text))
            {
                txtName.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtName.BackColor = Color.White;

            if (String.IsNullOrEmpty(txtDetails.Text))
            {
                txtDetails.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtDetails.BackColor = Color.White;

            if (!double.TryParse(txtPrice.Text, out double _))
            {
                txtPrice.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtPrice.BackColor = Color.White;

            if (!int.TryParse(txtQuantity.Text, out int _))
            {
                txtQuantity.BackColor = Color.Red;
                isValid = false;
            }
            else
                txtQuantity.BackColor = Color.White;

            return isValid;
        }

        private void ClearFormFields(TextBox txtName, TextBox txtDetails, TextBox txtPrice, TextBox txtQuantity)
        {
            txtName.Clear();
            txtDetails.Clear();
            txtPrice.Clear();
            txtQuantity.Clear();
        }        
    }
}
