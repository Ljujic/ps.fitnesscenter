﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmUserIndex : Form
    {
        public FrmUserIndex()
        {
            InitializeComponent();
        }

        private void btnFitnessProgramReservation_Click(object sender, EventArgs e)
        {
            FrmReserveProgram frmReserveProgram = new FrmReserveProgram();
            frmReserveProgram.Show();
        }

        private void btnCreatePurchaseOrder_Click(object sender, EventArgs e)
        {
            FrmUserPurchaseOrder frmUserPurchaseOrder = new FrmUserPurchaseOrder();
            frmUserPurchaseOrder.Show();
        }

        private void btnCancellationTerms_Click(object sender, EventArgs e)
        {
            FrmCancellationOfReservedTerm frmCancellationOfReservedTerm = new FrmCancellationOfReservedTerm();
            frmCancellationOfReservedTerm.Show();
        }

        private void btnPayPurchaseOrder_Click(object sender, EventArgs e)
        {
            FrmPayPurchaseOrder frmPayPurchaseOrder = new FrmPayPurchaseOrder();
            frmPayPurchaseOrder.Show();
        }
    }
}
