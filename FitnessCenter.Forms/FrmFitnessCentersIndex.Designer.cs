﻿namespace FitnessCenter.Forms
{
    partial class FrmFitnessCentersIndex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvFitnessCenters = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDeleteFitnessCenter = new System.Windows.Forms.Button();
            this.btnCreateFitnessCenter = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFitnessCenters)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFitnessCenters
            // 
            this.dgvFitnessCenters.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvFitnessCenters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFitnessCenters.Location = new System.Drawing.Point(12, 85);
            this.dgvFitnessCenters.Name = "dgvFitnessCenters";
            this.dgvFitnessCenters.Size = new System.Drawing.Size(776, 225);
            this.dgvFitnessCenters.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(360, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Fitness centers";
            // 
            // btnDeleteFitnessCenter
            // 
            this.btnDeleteFitnessCenter.BackColor = System.Drawing.SystemColors.ControlText;
            this.btnDeleteFitnessCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteFitnessCenter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDeleteFitnessCenter.Location = new System.Drawing.Point(578, 316);
            this.btnDeleteFitnessCenter.Name = "btnDeleteFitnessCenter";
            this.btnDeleteFitnessCenter.Size = new System.Drawing.Size(210, 37);
            this.btnDeleteFitnessCenter.TabIndex = 2;
            this.btnDeleteFitnessCenter.Text = "Delete fitness center";
            this.btnDeleteFitnessCenter.UseVisualStyleBackColor = false;
            this.btnDeleteFitnessCenter.Click += new System.EventHandler(this.btnDeleteFitnessCenter_Click);
            // 
            // btnCreateFitnessCenter
            // 
            this.btnCreateFitnessCenter.BackColor = System.Drawing.SystemColors.ControlText;
            this.btnCreateFitnessCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateFitnessCenter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCreateFitnessCenter.Location = new System.Drawing.Point(12, 12);
            this.btnCreateFitnessCenter.Name = "btnCreateFitnessCenter";
            this.btnCreateFitnessCenter.Size = new System.Drawing.Size(178, 30);
            this.btnCreateFitnessCenter.TabIndex = 3;
            this.btnCreateFitnessCenter.Text = "Create fitness center";
            this.btnCreateFitnessCenter.UseVisualStyleBackColor = false;
            this.btnCreateFitnessCenter.Click += new System.EventHandler(this.btnCreateFitnessCenter_Click);
            // 
            // FrmFitnessCentersIndex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnCreateFitnessCenter);
            this.Controls.Add(this.btnDeleteFitnessCenter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvFitnessCenters);
            this.Name = "FrmFitnessCentersIndex";
            this.Text = "Fitness Centers";
            ((System.ComponentModel.ISupportInitialize)(this.dgvFitnessCenters)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFitnessCenters;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDeleteFitnessCenter;
        private System.Windows.Forms.Button btnCreateFitnessCenter;
    }
}