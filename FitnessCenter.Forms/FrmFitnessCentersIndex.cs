﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmFitnessCentersIndex : Form
    {
        public FrmFitnessCentersIndex()
        {
            InitializeComponent();
            ArrangeForm();
        }

        private void ArrangeForm()
        {
            try
            {
                if (Communication.Instance.ConnectWithServer())
                {
                    List<FitnessCenterEntity> fitnessCenters = Communication.Instance.ReadFitnessCenters();
                    dgvFitnessCenters.DataSource = fitnessCenters;

                    if (fitnessCenters == null)
                        MessageBox.Show("System cannot read fitness centers! Try again!");
                }
                else
                {
                    MessageBox.Show("Server is not available! Try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCreateFitnessCenter_Click(object sender, EventArgs e)
        {
            FrmNewFitnessCenter frmNewFitnessCenter = new FrmNewFitnessCenter();
            frmNewFitnessCenter.Show();
            this.Hide();
        }

        private void btnDeleteFitnessCenter_Click(object sender, EventArgs e)
        {

            try
            {
                if (Communication.Instance.ConnectWithServer())
                {
                    if (dgvFitnessCenters.Rows.Count > 0)
                    {
                        FitnessCenterEntity fitnessCenter = (FitnessCenterEntity)dgvFitnessCenters.SelectedRows[0].DataBoundItem;
                        bool isDeleted = Communication.Instance.DeleteFitnessCenter(fitnessCenter.Id);

                        if (isDeleted)
                            MessageBox.Show("Fitness center successfully deleted!");
                        else
                            MessageBox.Show("Error! System cannot delete fitness center! Try again!");

                        dgvFitnessCenters.DataSource = Communication.Instance.ReadFitnessCenters();
                    }
                }
                else
                {
                    MessageBox.Show("Server is not available! Try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }
    }
}
