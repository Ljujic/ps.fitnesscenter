﻿using FitnessCenter.Domain;
using FitnessCenter.Forms.UserInterfaceControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmNewFitnessCenter : Form
    {
        private NewFitnessCenterController _controller = new NewFitnessCenterController();

        public FrmNewFitnessCenter()
        {
            InitializeComponent();
            ArrangeForm();
        }

        private void ArrangeForm()
        {
            cmbMunicipality.DataSource = Communication.Instance.ReadMunicipalities();
            cmbMunicipality.DisplayMember = "Name";
        }

        private void btnCreateFitnessCenter_Click(object sender, EventArgs e)
        {
            _controller.SaveFitnessCenter(cmbMunicipality, txtName, txtAddress, txtType, txtPhoneNumber, txtDetails);
        }

        private void btnAddProducts_Click(object sender, EventArgs e)
        {
            FrmAddFitnessCenterProducts frmAddFitnessCenterProducts = new FrmAddFitnessCenterProducts(_controller);
            frmAddFitnessCenterProducts.Show();
        }

        private void btnAddTrainers_Click(object sender, EventArgs e)
        {
            FrmAddFitnessCenterTrainers frmAddFitnessCenterTrainers = new FrmAddFitnessCenterTrainers(_controller);
            frmAddFitnessCenterTrainers.Show();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            FrmFitnessCentersIndex frmFitnessCentersIndex = new FrmFitnessCentersIndex();
            frmFitnessCentersIndex.Show();
            this.Close();
        }
    }
}
