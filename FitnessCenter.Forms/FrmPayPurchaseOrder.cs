﻿using FitnessCenter.Domain;
using FitnessCenter.Forms.UserInterfaceControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Forms
{
    public partial class FrmPayPurchaseOrder : Form
    {
        private readonly PayPurchaseOrderController _payPurchaseOrderController;

        public FrmPayPurchaseOrder()
        {
            InitializeComponent();

            _payPurchaseOrderController = new PayPurchaseOrderController();
            ArrangeForm();
        }

        private void ArrangeForm()
        {
            try
            {
                if (Communication.Instance.ConnectWithServer())
                {
                    List<PurchaseOrderEntity> purchaseOrders = Communication.Instance.ReadUserPurchaseOrder(Session.Instance.User);

                    dgvPurchaseOrders.DataSource = purchaseOrders;

                    dgvPurchaseOrders.Columns["Id"].Visible = false;
                    dgvPurchaseOrders.Columns["UserId"].Visible = false;
                    dgvPurchaseOrders.Columns["FitnessCenterId"].Visible = false;

                }
                else
                {
                    MessageBox.Show("Server is not available! Try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dgvPurchaseOrders.SelectedRows.Count > 0)
            {
                PurchaseOrderEntity pOrder = ((PurchaseOrderEntity)dgvPurchaseOrders.SelectedRows[0].DataBoundItem);

                if (Session.Instance.User.BankCardBalance >= pOrder.TotalPrice)
                {
                    bool isPaid = _payPurchaseOrderController.PayPurchaseOrder(pOrder);

                    if (isPaid)
                    { 
                        MessageBox.Show($"You successfully paid order!");
                        List<PurchaseOrderEntity> purchaseOrders = Communication.Instance.ReadUserPurchaseOrder(Session.Instance.User);
                        dgvPurchaseOrders.DataSource = purchaseOrders;                    
                    }


                }
                else
                {
                    MessageBox.Show("You don't have enough money to pay order!");
                }
            }
        }
    }
}
