﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Repository
{
    public interface IUpdateRepository<T> where T : IEntity
    {
        Task<T> UpdateAsync(T entity);
    }
}
