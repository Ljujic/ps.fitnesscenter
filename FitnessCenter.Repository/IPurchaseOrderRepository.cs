﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Repository
{
    public interface IPurchaseOrderRepository : IReadAllRepository<PurchaseOrderEntity>, IUpdateRepository<PurchaseOrderEntity>, IInsertRepository<PurchaseOrderEntity>
    {
        Task<IEnumerable<PurchaseOrderEntity>> ReadUserPurchaseOrdersAsync(long userId);
    }
}
