﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Repository
{
    public interface ITermRepository : IDeleteRepository<TermEntity>, IInsertRepository<TermEntity>
    {
        Task<IEnumerable<TermEntity>> FindUserTermsAsync(long userId, DateTime startDate, DateTime endDate);
    }
}
