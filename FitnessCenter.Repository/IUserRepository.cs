﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Repository
{
    public interface IUserRepository : IInsertRepository<UserEntity>
    {
        Task<UserEntity> PayPurchasePrice(PurchaseOrderEntity purchaseOrder);
        Task<UserEntity> FindUserByUsernameOrEmail(string username, string email);
    }
}
