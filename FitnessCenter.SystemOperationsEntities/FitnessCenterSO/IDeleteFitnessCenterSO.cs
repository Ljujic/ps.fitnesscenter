﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperationsEntities.FitnessCenterSO
{
    public interface IDeleteFitnessCenterSO
    {
        Task<bool> DeleteFitnessCenterAsync(long fitnessCenterId);
    }
}
