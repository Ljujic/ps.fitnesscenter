﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperationsEntities.FitnessCenterSO
{
    public interface IFitnessCenterFacade : IReadAllSO<FitnessCenterEntity>, IInsertFitnessCenterSO, IDeleteFitnessCenterSO
    {
    }
}
