﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperationsEntities.PurchaseOrderSO
{
    public interface IReadUserPurchaseOrdersSO
    {
        Task<List<PurchaseOrderEntity>> ReadPurchaseOrdersAsync(long userId);
    }
}
