﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperationsEntities.UserSO
{
    public interface IRegisterUserSO
    {
        Task<bool> RegisterUserAsync(UserEntity user);
    }
}
