﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperationsEntities.UserSO
{
    public interface IUserValidation : IValidation<UserEntity>
    {
        Task<bool> isValidUsernameAndEmail(UserEntity user);
    }
}
