﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperationsEntities.AuthenticationSO
{
    public interface IUserAuthenticate
    {
        Task<UserEntity> Authenticate(string username, string password);
    }
}
