﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperationsEntities.AuthenticationSO
{
    public interface IGeneratingHashPassword
    {
        string HashPassword(string pwd);
    }
}
