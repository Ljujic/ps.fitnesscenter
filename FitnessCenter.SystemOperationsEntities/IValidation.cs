﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperationsEntities
{
    public interface IValidation<T> where T : IEntity
    {
        bool IsValidEntity(T entity);
    }
}
