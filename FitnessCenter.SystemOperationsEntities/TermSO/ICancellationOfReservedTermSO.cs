﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperationsEntities.TermSO
{
    public interface ICancellationOfReservedTermSO
    {
        Task<bool> CancellationOfReservedTermAsync(long termId);
    }
}
