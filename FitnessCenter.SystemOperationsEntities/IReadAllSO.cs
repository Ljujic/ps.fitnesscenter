﻿using FitnessCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperationsEntities
{
    public interface IReadAllSO<T> where T : IEntity
    {
        Task<List<T>> ReadAllAsync();
    }
}
