﻿using FitnessCenter.Controllers.Entities;
using FitnessCenter.Server.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FitnessCenter.Server
{
    public class Server : IServer
    {
        private Socket _socket;

        private readonly IAuthenticationController _authenticationController;
        private readonly IFitnessCentersController _fitnessCentersController;
        private readonly IMunicipalitiesController _municipalitiesController;
        private readonly IProductsController _productsController;
        private readonly IProgramsController _programsController;
        private readonly IPurchaseOrdersController _purchaseOrdersController;
        private readonly ITermsController _termsController;
        private readonly IUsersController _usersController;

        public Server(
            IAuthenticationController authenticationController,
            IFitnessCentersController fitnessCentersController,
            IMunicipalitiesController municipalitiesController,
            IProductsController productsController,
            IProgramsController programsController,
            IPurchaseOrdersController purchaseOrdersController,
            ITermsController termsController,
            IUsersController usersController
            )
        {
            this._authenticationController = authenticationController;
            this._fitnessCentersController = fitnessCentersController;
            this._municipalitiesController = municipalitiesController;
            this._productsController = productsController;
            this._programsController = programsController;
            this._purchaseOrdersController = purchaseOrdersController;
            this._termsController = termsController;
            this._usersController = usersController;
        }

        public bool StartServer()
        {
            try
            {
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _socket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9090));
                _socket.Listen(5);

                return true;
            }
            catch (SocketException)
            {

                return false;
            }
        }

        public void Listen()
        {
            bool kraj = false;
            while (!kraj)
            {
                Socket clientSocket = _socket.Accept();
                RequestsManager requestsManager = new RequestsManager(clientSocket, _authenticationController, _fitnessCentersController, _municipalitiesController, _productsController, 
                    _programsController, _purchaseOrdersController, _termsController, _usersController);
                Thread clientThread = new Thread(requestsManager.ResolveRequest);
                clientThread.Start();
            }
        }
    }
}
