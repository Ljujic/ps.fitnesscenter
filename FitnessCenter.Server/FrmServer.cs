﻿using FitnessCenter.Server.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Server
{
    public partial class FrmServer : Form
    {
        private IServer _server;
        private Thread _thread;

        public FrmServer(IServer server)
        {
            InitializeComponent();
            this._server = server;
            btnStartServer.Enabled = true;
        }

        private void btnStartServer_Click(object sender, EventArgs e)
        {
            if (_server.StartServer())
            {
                _thread = new Thread(_server.Listen);
                _thread.IsBackground = true;
                _thread.Start();

                btnStartServer.Visible = false;
                txtServer.Text = "Server is started!";
                txtServer.Enabled = false;
            }
            else
            {
                txtServer.Text = "Server is not started!";
            }
        }
    }
}
