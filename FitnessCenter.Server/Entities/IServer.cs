﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Server.Entities
{
    public interface IServer
    {
        bool StartServer();
        void Listen();
    }
}
