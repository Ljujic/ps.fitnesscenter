﻿using FitnessCenter.Broker;
using FitnessCenter.Broker.Repository;
using FitnessCenter.Controllers;
using FitnessCenter.Controllers.Entities;
using FitnessCenter.Domain;
using FitnessCenter.Server.Entities;
using FitnessCenter.Server.Extensions;
using FitnessCenter.SystemOperations;
using FitnessCenter.SystemOperations.AuthenticationSO;
using FitnessCenter.SystemOperations.FitnessCenterSO;
using FitnessCenter.SystemOperations.PurchaseOrderSO;
using FitnessCenter.SystemOperations.TermSO;
using FitnessCenter.SystemOperations.TermSO.ReserveProgramTermSO;
using FitnessCenter.SystemOperations.UserSO;
using FitnessCenter.SystemOperationsEntities;
using FitnessCenter.SystemOperationsEntities.AuthenticationSO;
using FitnessCenter.SystemOperationsEntities.FitnessCenterSO;
using FitnessCenter.SystemOperationsEntities.PurchaseOrderSO;
using FitnessCenter.SystemOperationsEntities.TermSO;
using FitnessCenter.SystemOperationsEntities.UserSO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessCenter.Server
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var provider = BuildServices();

            using (var scope = provider.CreateScope())
            {
                var server = scope.ServiceProvider.GetService<IServer>();
                Application.Run(new FrmServer(server));
            }

        }

        private static IServiceProvider BuildServices()
        {
            var services = new ServiceCollection();
            var configuration = BuildConfiguration();

            services.Configure<DbSettings>(configuration.GetSection("DbSettings"));

            //registration of system operations
            services.AddTransient<IReadAllSO<MunicipalityEntity>, ReadMunicipalities>();
            services.AddTransient<IReadAllSO<ProductEntity>, ReadProducts>();
            services.AddTransient<IReadAllSO<ProgramEntity>, ReadPrograms>();

            services.AddTransient<IReadAllSO<FitnessCenterEntity>, ReadFitnessCenters>();
            services.AddTransient<IDeleteFitnessCenterSO, DeleteFitnessCenter>();
            services.AddTransient<IValidation<FitnessCenterEntity>, FitnessCenterEntityValidation>();
            services.AddTransient<IInsertFitnessCenterSO, FitnessCenterInsert>();
            services.Decorate<IInsertFitnessCenterSO, FitnessCenterInsertValidation>();
            services.AddTransient<IFitnessCenterFacade, FitnessCenterFacade>();

            services.AddTransient<IValidation<PurchaseOrderEntity>, PurchaseOrderEntityValidation>();
            services.AddTransient<IInsertUserPurchaseOrderSO, PurchaseOrderInsert>();
            services.Decorate<IInsertUserPurchaseOrderSO, PurchaseOrderInsertValidation>();
            services.AddTransient<IUpdatePurchaseOrderSO, PurchaseOrderUpdate>();
            services.Decorate<IUpdatePurchaseOrderSO, PurchaseOrderUpdateValidation>();
            services.AddTransient<IReadUserPurchaseOrdersSO, ReadUserPurchaseOrders>();

            services.AddTransient<ICancellationOfReservedTermSO, CancellationOfReservedTerm>();
            services.AddTransient<IFindUserTermsSO, FindUserTerms>();
            services.AddTransient<IValidation<TermEntity>, ProgramEntityValidation>();
            services.AddTransient<IReserveProgramTermSO, ReserveProgramTerm>();
            services.Decorate<IReserveProgramTermSO, ReserveProgramTermValidation>();

            services.AddTransient<IUserValidation, UserEntityValidation>();
            services.AddTransient<IPayPurchaseOrderSO, PayPurchaseOrder>();
            services.AddTransient<IRegisterUserSO, UserRegister>();
            services.Decorate<IRegisterUserSO, UserRegisterValidation>();

            services.AddTransient<IGeneratingHashPassword, GeneratingHashPassword>();
            services.AddTransient<IUserAuthenticate, UserAuthenticate>();

            services.AddSingleton<IFitnessCentersController, FitnessCentersController>();
            services.AddSingleton<IMunicipalitiesController, MunicipalitiesController>();
            services.AddSingleton<IProductsController, ProductsController>();
            services.AddSingleton<IProgramsController, ProgramsController>();
            services.AddSingleton<IPurchaseOrdersController, PurchaseOrdersController>();
            services.AddSingleton<ITermsController, TermsController>();
            services.AddSingleton<IUsersController, UsersController>();
            services.AddSingleton<IAuthenticationController, AuthenticationController>();

            services.AddTransient<IServer, Server>();


            RegisterRepository(services);


            return services.BuildServiceProvider();
        }

        private static void RegisterRepository(IServiceCollection services)
        {
            services.Scan(scan => scan.FromAssemblyOf<ScrutorRegistrationMarker>()
                                      .AddClasses(classes => classes.AssignableTo<BaseSqlRepository>())
                                      .AsFirstInheritedInterface()
                                      .WithScopedLifetime());
        }

        private static IConfiguration BuildConfiguration()
        {
            var file = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            return new ConfigurationBuilder()
                    .AddJsonFile(file, optional: false, reloadOnChange: true)
                    .Build();
        }
    }
}
