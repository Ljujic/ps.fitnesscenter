﻿using Scrutor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Server.Extensions
{
    public static class ScrutorExtensions
    {
        public static ILifetimeSelector AsFirstInheritedInterface(this IServiceTypeSelector obj)
        {
            return obj.As(type =>
            {
                var firstInterface = type.GetInterfaces()[0];
                return new List<Type>() { firstInterface };
            });
        }
    }
}
