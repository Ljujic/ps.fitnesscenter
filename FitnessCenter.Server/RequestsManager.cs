﻿using FitnessCenter.Controllers.Entities;
using FitnessCenter.Domain;
using FitnessCenter.Server.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.Server
{
    public class RequestsManager
    {
        private Socket _clientSocket;
        private NetworkStream _stream;
        private BinaryFormatter _formatter = new BinaryFormatter();

        private readonly IAuthenticationController _authenticationController;
        private readonly IFitnessCentersController _fitnessCentersController;
        private readonly IMunicipalitiesController _municipalitiesController;
        private readonly IProductsController _productsController;
        private readonly IProgramsController _programsController;
        private readonly IPurchaseOrdersController _purchaseOrdersController;
        private readonly ITermsController _termsController;
        private readonly IUsersController _usersController;

        public RequestsManager(Socket clientSocket,
                               IAuthenticationController authenticationController,
                               IFitnessCentersController fitnessCentersController,
                               IMunicipalitiesController municipalitiesController,
                               IProductsController productsController,
                               IProgramsController programsController,
                               IPurchaseOrdersController purchaseOrdersController,
                               ITermsController termsController,
                               IUsersController usersController)
        {
            this._clientSocket = clientSocket;
            _stream = new NetworkStream(this._clientSocket);

            this._authenticationController = authenticationController;
            this._fitnessCentersController = fitnessCentersController;
            this._municipalitiesController = municipalitiesController;
            this._productsController = productsController;
            this._programsController = programsController;
            this._purchaseOrdersController = purchaseOrdersController;
            this._termsController = termsController;
            this._usersController = usersController;
        }

        public async void ResolveRequest()
        {
            bool kraj = false;
            while (!kraj)
            {
                try
                {
                    Request request = (Request)_formatter.Deserialize(_stream);
                    Response response = await ExecuteRequest(request);
                    _formatter.Serialize(_stream, response);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($">>> {ex.Message}");
                    kraj = true;
                }
            }
        }

        private async Task<Response> ExecuteRequest(Request request)
        {
            Response response = new Response();

            try
            {
                switch (request.Operation)
                {
                    case OPERATION.Login:
                        UserEntity loginUser = (UserEntity)request.Object;
                        response.Object = await _authenticationController.Login(loginUser.Username, loginUser.Password);
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.ReadMunicipalities:
                        response.Object = await _municipalitiesController.ReadAllAsync();
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.ReadPrograms:
                        response.Object = await _programsController.ReadAllAsync();
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.CreateFitnessCenter:
                        FitnessCenterEntity fitnessCenter = (FitnessCenterEntity)request.Object;
                        response.Object = await _fitnessCentersController.CreateFitnessCenterAsync(fitnessCenter);
                        response.Message = "Fitness center inserted successfully!";
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.ReadFitnessCenters:
                        response.Object = await _fitnessCentersController.ReadAllAsync();
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.DeleteFitnesCenter:
                        long fitnessCenterId = (long)request.Object;
                        response.Object = await _fitnessCentersController.DeleteFitnessCenterAsync(fitnessCenterId);
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.RegisterUser:
                        UserEntity user = (UserEntity)request.Object;
                        response.Object = await _usersController.RegisterUserAsync(user);
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.ReserveProgram:
                        TermEntity programTerm = (TermEntity)request.Object;
                        response.Object = await _termsController.ReserveProgramTermAsync(programTerm);
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.ReadProducts:
                        response.Object = await _productsController.ReadAllAsync();
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.CreatePurchaseOrder:
                        PurchaseOrderEntity purchaseOrderEntity = (PurchaseOrderEntity)request.Object;
                        response.Object = await _purchaseOrdersController.InsertPurchaseOrderAsync(purchaseOrderEntity);
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.UpdatePurchaseOrder:
                        PurchaseOrderEntity purchaseOrder = (PurchaseOrderEntity)request.Object;
                        response.Object = await _purchaseOrdersController.UpdatePurchaseOrderAsync(purchaseOrder);
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.ReadPurchaseOrder:
                        long userId = (long)request.Object;
                        response.Object = await _purchaseOrdersController.ReadUserPurchaseOrdersAsync(userId);
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.PayPurchasePrice:
                        PurchaseOrderEntity payPurchaseOrderEntity = (PurchaseOrderEntity)request.Object;
                        response.Object = await _usersController.PayPurchaseOrderAsync(payPurchaseOrderEntity);
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.FindUserTerms:
                        TermEntity term = (TermEntity)request.Object;
                        response.Object = await _termsController.FindUserTermsAsync(term.UserId, term.DateOfTerm, term.EndDate);
                        response.Signal = SIGNAL.Ok;
                        break;
                    case OPERATION.CancellationOfReservedTerm:
                        long termId = (long)request.Object;
                        response.Object = await _termsController.CancellationOfReservedTermAsync(termId);
                        response.Signal = SIGNAL.Ok;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                response.Signal = SIGNAL.Error;
                response.Message = e.Message;
                throw;
            }

            return response;
        }
    }
}
