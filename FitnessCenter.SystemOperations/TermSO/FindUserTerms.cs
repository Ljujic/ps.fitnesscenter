﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities.TermSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.TermSO
{
    public class FindUserTerms : IFindUserTermsSO
    {
        private readonly ITermRepository _termRepository;

        public FindUserTerms(ITermRepository termRepository)
        {
            this._termRepository = termRepository;
        }

        public async Task<List<TermEntity>> FindUserTermsAsync(long userId, DateTime startDate, DateTime endDate)
        {
            IEnumerable<TermEntity> userTerms = await _termRepository.FindUserTermsAsync(userId, startDate, endDate);

            return (List<TermEntity>)userTerms;
        }
    }
}
