﻿using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities.TermSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.TermSO
{
    public class CancellationOfReservedTerm : ICancellationOfReservedTermSO
    {
        private readonly ITermRepository _termRepository;

        public CancellationOfReservedTerm(ITermRepository termRepository)
        {
            this._termRepository = termRepository;
        }

        public async Task<bool> CancellationOfReservedTermAsync(long termId)
        {
            var isCancelled = await _termRepository.DeleteAsync(termId);

            return isCancelled;
        }
    }
}
