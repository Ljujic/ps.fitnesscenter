﻿using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using FitnessCenter.SystemOperationsEntities.TermSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.TermSO.ReserveProgramTermSO
{
    public class ReserveProgramTermValidation : IReserveProgramTermSO
    {
        private readonly IReserveProgramTermSO _reserveProgramTerm;
        private readonly IValidation<TermEntity> _validation;

        public ReserveProgramTermValidation(IReserveProgramTermSO reserveProgramTerm, IValidation<TermEntity> validation)
        {
            this._reserveProgramTerm = reserveProgramTerm;
            this._validation = validation;
        }

        public async Task<bool> ReserveProgramTermAsync(TermEntity term)
        {
            var isValidTerm = _validation.IsValidEntity(term);

            if (!isValidTerm)
                return false;

            return await _reserveProgramTerm.ReserveProgramTermAsync(term);
        }
    }
}
