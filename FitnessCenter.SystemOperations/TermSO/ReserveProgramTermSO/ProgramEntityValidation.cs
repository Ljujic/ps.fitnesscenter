﻿using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.TermSO.ReserveProgramTermSO
{
    public class ProgramEntityValidation : IValidation<TermEntity>
    {
        public bool IsValidEntity(TermEntity entity)
        {
            var isValid = true;

            if ((entity.DateOfTerm.Year < DateTime.Now.Year) || 
                (entity.DateOfTerm.Year==DateTime.Now.Year && entity.DateOfTerm.Month < DateTime.Now.Month) ||
                (entity.DateOfTerm.Year == DateTime.Now.Year && entity.DateOfTerm.Month==DateTime.Now.Month && entity.DateOfTerm.Day < DateTime.Now.Day))
                isValid = false;
            
            return isValid;
        }
    }
}
