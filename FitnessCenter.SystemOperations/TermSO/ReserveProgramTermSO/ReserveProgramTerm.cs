﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities.TermSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.TermSO.ReserveProgramTermSO
{
    public class ReserveProgramTerm : IReserveProgramTermSO
    {
        private readonly ITermRepository _termRepository;

        public ReserveProgramTerm(ITermRepository termRepository)
        {
            this._termRepository = termRepository;
        }

        public async Task<bool> ReserveProgramTermAsync(TermEntity term)
        {
            TermEntity newTerm = await _termRepository.InsertAsync(term);

            return newTerm != null;
        }
    }
}
