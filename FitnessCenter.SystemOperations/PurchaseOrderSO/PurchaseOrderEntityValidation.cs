﻿using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.PurchaseOrderSO
{
    public class PurchaseOrderEntityValidation : IValidation<PurchaseOrderEntity>
    {
        public bool IsValidEntity(PurchaseOrderEntity entity)
        {
            var isValid = true;

            if (entity.DateOfDispatch < DateTime.Now)
                isValid = false;
            
            if (entity.TotalPrice < 0)
                isValid = false;

            return isValid;
        }
    }
}
