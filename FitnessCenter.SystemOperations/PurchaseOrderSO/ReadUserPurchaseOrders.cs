﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities.PurchaseOrderSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.PurchaseOrderSO
{
    public class ReadUserPurchaseOrders : IReadUserPurchaseOrdersSO
    {
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;

        public ReadUserPurchaseOrders(IPurchaseOrderRepository purchaseOrderRepository)
        {
            this._purchaseOrderRepository = purchaseOrderRepository;
        }

        public async Task<List<PurchaseOrderEntity>> ReadPurchaseOrdersAsync(long userId)
        {
            var userPurchaseOrders = await _purchaseOrderRepository.ReadUserPurchaseOrdersAsync(userId);

            return (List<PurchaseOrderEntity>)userPurchaseOrders;
        }
    }
}
