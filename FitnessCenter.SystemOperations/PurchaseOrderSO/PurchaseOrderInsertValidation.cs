﻿using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using FitnessCenter.SystemOperationsEntities.PurchaseOrderSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.PurchaseOrderSO
{
    public class PurchaseOrderInsertValidation : IInsertUserPurchaseOrderSO
    {
        private readonly IInsertUserPurchaseOrderSO _insertUserPurchaseOrder;
        private IValidation<PurchaseOrderEntity> _validation;

        public PurchaseOrderInsertValidation(IInsertUserPurchaseOrderSO insertUserPurchaseOrder, IValidation<PurchaseOrderEntity> validation)
        {
            this._insertUserPurchaseOrder = insertUserPurchaseOrder;
            this._validation = validation;
        }

        public async Task<bool> InsertUserPurchaseOrder(PurchaseOrderEntity purchaseOrder)
        {
            var isValidEntity = _validation.IsValidEntity(purchaseOrder);

            if (!isValidEntity)
                return false;

            return await _insertUserPurchaseOrder.InsertUserPurchaseOrder(purchaseOrder);
        }
    }
}
