﻿using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using FitnessCenter.SystemOperationsEntities.PurchaseOrderSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.PurchaseOrderSO
{
    public class PurchaseOrderUpdateValidation : IUpdatePurchaseOrderSO
    {
        private readonly IUpdatePurchaseOrderSO _updatePurchaseOrder;
        private IValidation<PurchaseOrderEntity> _validation;

        public PurchaseOrderUpdateValidation(IUpdatePurchaseOrderSO updatePurchaseOrder, IValidation<PurchaseOrderEntity> validation)
        {
            this._updatePurchaseOrder = updatePurchaseOrder;
            this._validation = validation;
        }

        public async Task<bool> UpdatePurchaseOrderAsync(PurchaseOrderEntity purchaseOrder)
        {
            var isValidEntity = _validation.IsValidEntity(purchaseOrder);

            if (!isValidEntity)
                return false;

            return await _updatePurchaseOrder.UpdatePurchaseOrderAsync(purchaseOrder);
        }
    }
}
