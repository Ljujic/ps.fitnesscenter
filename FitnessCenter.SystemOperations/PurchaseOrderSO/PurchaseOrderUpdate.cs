﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities.PurchaseOrderSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.PurchaseOrderSO
{
    public class PurchaseOrderUpdate : IUpdatePurchaseOrderSO
    {
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;

        public PurchaseOrderUpdate(IPurchaseOrderRepository purchaseOrderRepository)
        {
            this._purchaseOrderRepository = purchaseOrderRepository;
        }

        public async Task<bool> UpdatePurchaseOrderAsync(PurchaseOrderEntity purchaseOrder)
        {
            PurchaseOrderEntity purchaseOrderEntity = await _purchaseOrderRepository.UpdateAsync(purchaseOrder);

            return purchaseOrderEntity != null;
        }
    }
}
