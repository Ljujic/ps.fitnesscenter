﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities.PurchaseOrderSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.PurchaseOrderSO
{
    public class PurchaseOrderInsert : IInsertUserPurchaseOrderSO
    {
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;

        public PurchaseOrderInsert(IPurchaseOrderRepository purchaseOrderRepository)
        {
            this._purchaseOrderRepository = purchaseOrderRepository;
        }

        public async Task<bool> InsertUserPurchaseOrder(PurchaseOrderEntity purchaseOrder)
        {
            PurchaseOrderEntity purchaseOrderEntity = await _purchaseOrderRepository.InsertAsync(purchaseOrder);

            return purchaseOrderEntity != null;
        }
    }
}
