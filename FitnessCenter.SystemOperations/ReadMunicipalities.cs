﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations
{
    public class ReadMunicipalities : IReadAllSO<MunicipalityEntity>
    {
        private readonly IMunicipalityRepository _municipalityRepository;

        public ReadMunicipalities(IMunicipalityRepository municipalityRepository)
        {
            this._municipalityRepository = municipalityRepository;
        }

        public async Task<List<MunicipalityEntity>> ReadAllAsync()
        {
            IEnumerable<MunicipalityEntity> municipalities = await _municipalityRepository.GetAllAsync();

            return (List<MunicipalityEntity>)municipalities;
        }
    }
}
