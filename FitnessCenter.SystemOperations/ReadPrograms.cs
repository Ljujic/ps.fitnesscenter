﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations
{
    public class ReadPrograms : IReadAllSO<ProgramEntity>
    {
        private readonly IProgramRepository _programRepository;

        public ReadPrograms(IProgramRepository programRepository)
        {
            this._programRepository = programRepository;
        }

        public async Task<List<ProgramEntity>> ReadAllAsync()
        {
            IEnumerable<ProgramEntity> programs = await _programRepository.GetAllAsync();

            return (List<ProgramEntity>)programs;
        }
    }
}
