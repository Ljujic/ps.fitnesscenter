﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities.AuthenticationSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.AuthenticationSO
{
    public class UserAuthenticate : IUserAuthenticate
    {
        private readonly IUserRepository _userRepository;

        public UserAuthenticate(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public async Task<UserEntity> Authenticate(string username, string password)
        {
            var user = await _userRepository.FindUserByUsernameOrEmail(username, "");

            if (user != null && user.Password == password)
                return user;

            return null;
        }
    }
}
