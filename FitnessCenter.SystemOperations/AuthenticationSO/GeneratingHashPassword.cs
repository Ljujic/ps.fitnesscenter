﻿using FitnessCenter.SystemOperationsEntities.AuthenticationSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.AuthenticationSO
{
    public class GeneratingHashPassword : IGeneratingHashPassword
    {
        public string HashPassword(string pwd)
        {
            var bytes = Encoding.UTF8.GetBytes(pwd);
            var result = "";
            using (var hash = System.Security.Cryptography.SHA512.Create())
            {
                var hashedBytes = hash.ComputeHash(bytes);
                result = BitConverter.ToString(hashedBytes).Replace("-", "");
            }

            return result;
        }
    }
}
