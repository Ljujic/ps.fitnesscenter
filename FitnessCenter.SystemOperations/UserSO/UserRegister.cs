﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities;
using FitnessCenter.SystemOperationsEntities.UserSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.UserSO
{
    public class UserRegister : IRegisterUserSO
    {
        private IUserRepository _userRepository;

        public UserRegister(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public async Task<bool> RegisterUserAsync(UserEntity user)
        {
            var newUser = await _userRepository.InsertAsync(user);

            return newUser != null;
        }
    }
}
