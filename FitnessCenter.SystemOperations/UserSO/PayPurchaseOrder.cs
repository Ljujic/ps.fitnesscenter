﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities.UserSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.UserSO
{
    public class PayPurchaseOrder : IPayPurchaseOrderSO
    {
        private readonly IUserRepository _userRepository;

        public PayPurchaseOrder(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public async Task<bool> PayPurchaseOrderAsync(PurchaseOrderEntity purchaseOrder)
        {
            UserEntity user = await _userRepository.PayPurchasePrice(purchaseOrder);

            return user != null;
        }
    }
}
