﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities;
using FitnessCenter.SystemOperationsEntities.UserSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.UserSO
{
    public class UserEntityValidation : IUserValidation
    {
        private readonly IUserRepository _userRepository;

        public UserEntityValidation(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public bool IsValidEntity(UserEntity entity)
        {
            var isValid = true;

            if (String.IsNullOrEmpty(entity.Username))
                isValid = false;
            if (String.IsNullOrEmpty(entity.Password) || entity.Password.Length < 6)
                isValid = false;
            if (String.IsNullOrEmpty(entity.NameSurname))
                isValid = false;
            if (String.IsNullOrEmpty(entity.Email))
                isValid = false;
            if ((entity.DateOfBirth.Year < DateTime.Now.Year) ||
                (entity.DateOfBirth.Year == DateTime.Now.Year && entity.DateOfBirth.Month < DateTime.Now.Month) ||
                (entity.DateOfBirth.Year == DateTime.Now.Year && entity.DateOfBirth.Month == DateTime.Now.Month && entity.DateOfBirth.Day < DateTime.Now.Day))
                isValid = false;

            return isValid;
        }

        public async Task<bool> isValidUsernameAndEmail(UserEntity user)
        {
            return await _userRepository.FindUserByUsernameOrEmail(user.Username, user.Email) != null;
        }
    }
}
