﻿using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities.UserSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.UserSO
{
    public class UserRegisterValidation : IRegisterUserSO
    {
        private readonly IRegisterUserSO _registerUser;
        private readonly IUserValidation _validation;

        public UserRegisterValidation(IRegisterUserSO registerUser, IUserValidation validation)
        {
            this._registerUser = registerUser;
            this._validation = validation;
        }

        public async Task<bool> RegisterUserAsync(UserEntity user)
        {
            var isValidEntity = _validation.IsValidEntity(user);
            var existUserWithEmailUsername = await _validation.isValidUsernameAndEmail(user);

            if (!isValidEntity || existUserWithEmailUsername)
                return false;

            return await _registerUser.RegisterUserAsync(user);
        }
    }
}
