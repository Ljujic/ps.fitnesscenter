﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations
{
    public class ReadProducts : IReadAllSO<ProductEntity>
    {
        private readonly IProductRepository _productRepository;

        public ReadProducts(IProductRepository productRepository)
        {
            this._productRepository = productRepository;
        }

        public async Task<List<ProductEntity>> ReadAllAsync()
        {
            IEnumerable<ProductEntity> products = await _productRepository.GetAllAsync();

            return (List<ProductEntity>)products;
        }
    }
}
