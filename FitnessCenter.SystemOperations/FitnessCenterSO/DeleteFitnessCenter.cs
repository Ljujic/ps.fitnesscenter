﻿using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities.FitnessCenterSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.FitnessCenterSO
{
    public class DeleteFitnessCenter : IDeleteFitnessCenterSO
    {
        private readonly IFitnessCenterRepository _fitnessCenterRepository;

        public DeleteFitnessCenter(IFitnessCenterRepository fitnessCenterRepository)
        {
            this._fitnessCenterRepository = fitnessCenterRepository;
        }

        public async Task<bool> DeleteFitnessCenterAsync(long fitnessCenterId)
        {
            var isDeleted = await _fitnessCenterRepository.DeleteAsync(fitnessCenterId);

            return isDeleted;
        }
    }
}
