﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.FitnessCenterSO
{
    public class ReadFitnessCenters : IReadAllSO<FitnessCenterEntity>
    {
        private readonly IFitnessCenterRepository _fitnessCenterRepository;

        public ReadFitnessCenters(IFitnessCenterRepository fitnessCenterRepository)
        {
            this._fitnessCenterRepository = fitnessCenterRepository;
        }

        public async Task<List<FitnessCenterEntity>> ReadAllAsync()
        {
            IEnumerable<FitnessCenterEntity> fitnessCenters = await _fitnessCenterRepository.GetAllAsync();

            return (List<FitnessCenterEntity>)fitnessCenters;
        }
    }
}
