﻿using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.FitnessCenterSO
{
    public class FitnessCenterEntityValidation : IValidation<FitnessCenterEntity>
    {

        public bool IsValidEntity(FitnessCenterEntity entity)
        {
            var isValid = true;

            if (String.IsNullOrEmpty(entity.Name))
                isValid = false;
            if (String.IsNullOrEmpty(entity.Address))
                isValid = false;
            if (String.IsNullOrEmpty(entity.PhoneNumber))
                isValid = false;
            if (String.IsNullOrEmpty(entity.Type))
                isValid = false;
            if (String.IsNullOrEmpty(entity.Details))
                isValid = false;
            if (entity.Trainers.Count == 0 || entity.Trainers == null)
                isValid = false;

            return isValid;
        }
    }
}
