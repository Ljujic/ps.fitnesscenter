﻿using FitnessCenter.Domain;
using FitnessCenter.Repository;
using FitnessCenter.SystemOperationsEntities.FitnessCenterSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.FitnessCenterSO
{
    public class FitnessCenterInsert : IInsertFitnessCenterSO
    {
        private readonly IFitnessCenterRepository _fitnessCenterRepository;

        public FitnessCenterInsert(IFitnessCenterRepository fitnessCenterRepository)
        {
            this._fitnessCenterRepository = fitnessCenterRepository;
        }

        public async Task<bool> CreateFitnessCenterAsync(FitnessCenterEntity fitnessCenter)
        {
            var newFitnessCenter = await _fitnessCenterRepository.InsertAsync(fitnessCenter);

            return newFitnessCenter != null;
        }
    }
}
