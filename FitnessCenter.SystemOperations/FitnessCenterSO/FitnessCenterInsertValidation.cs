﻿using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using FitnessCenter.SystemOperationsEntities.FitnessCenterSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.FitnessCenterSO
{
    public class FitnessCenterInsertValidation : IInsertFitnessCenterSO
    {
        private IInsertFitnessCenterSO _insertFitnessCenter;
        private IValidation<FitnessCenterEntity> _validation;

        public FitnessCenterInsertValidation(IInsertFitnessCenterSO insertFitnessCenter, IValidation<FitnessCenterEntity> validation)
        {
            this._insertFitnessCenter = insertFitnessCenter;
            this._validation = validation;
        }

        public async Task<bool> CreateFitnessCenterAsync(FitnessCenterEntity fitnessCenter)
        {
            var isValidEntity = _validation.IsValidEntity(fitnessCenter);

            if (!isValidEntity)
                return false;

            return await _insertFitnessCenter.CreateFitnessCenterAsync(fitnessCenter);
        }
    }
}
