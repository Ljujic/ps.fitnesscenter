﻿using FitnessCenter.Domain;
using FitnessCenter.SystemOperationsEntities;
using FitnessCenter.SystemOperationsEntities.FitnessCenterSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessCenter.SystemOperations.FitnessCenterSO
{
    public class FitnessCenterFacade : IFitnessCenterFacade
    {
        private IReadAllSO<FitnessCenterEntity> _readAll;
        private readonly IInsertFitnessCenterSO _insertFitnessCenter;
        private readonly IDeleteFitnessCenterSO _deleteFitnessCenter;

        public FitnessCenterFacade(IReadAllSO<FitnessCenterEntity> readAll, IInsertFitnessCenterSO insertFitnessCenter, IDeleteFitnessCenterSO deleteFitnessCenter)
        {
            this._readAll = readAll;
            this._insertFitnessCenter = insertFitnessCenter;
            this._deleteFitnessCenter = deleteFitnessCenter;
        }

        public async Task<bool> CreateFitnessCenterAsync(FitnessCenterEntity fitnessCenter)
        {
            return await _insertFitnessCenter.CreateFitnessCenterAsync(fitnessCenter);
        }

        public async Task<bool> DeleteFitnessCenterAsync(long fitnessCenterId)
        {
            return await _deleteFitnessCenter.DeleteFitnessCenterAsync(fitnessCenterId);
        }

        public async Task<List<FitnessCenterEntity>> ReadAllAsync()
        {
            return await _readAll.ReadAllAsync();
        }
    }
}
